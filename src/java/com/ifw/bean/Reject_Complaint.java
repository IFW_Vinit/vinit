/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.bean;

import java.util.Date;

/**
 *
 * @author heena
 */
public class Reject_Complaint {
     private String comp_id;
    private Date reject_date;
    private String reason;
    private String suggestion;

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    public Date getReject_date() {
        return reject_date;
    }

    public void setReject_date(Date reject_date) {
        this.reject_date = reject_date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }
    
   
}
