/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import com.ifw.Utils.*;

/**
 *
 * @author Yash
 */
public class DBMS extends Constants {

    // private static DataSource ds;
    private Connection conn = null;
    private Statement st = null;
    private ResultSet rs = null;

//    static {
//        try {
//            InitialContext ctx = new InitialContext();
//            ds = (DataSource) ctx.lookup("jdbc/myDatasource");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    //jdbc/myDatasource
    private void estConnection() {
        getProperties();
        try {
            Class.forName(Constants.DBMS_DRIVER);
            conn = DriverManager.getConnection(Constants.DBMS_URL, Constants.DBMS_USERNAME, Constants.DBMS_PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        try {
//            conn = ds.getConnection();
//            if(conn!=null){
//                System.out.println("Connection Established !!!");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public void closeConnection() {
//        try {
//            conn.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                System.out.println(
                        "SQL Exception in closing resultset"+
                        e);
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                System.out.println(
                        "SQL Exception in closing statement"+
                        e);
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
               System.out.println(
                        "SQL Exception in closing connection"+
                        e);
            }
        }

    }

    public int guid(String sql) {
        int ru = 0;
        estConnection();
        try {
            st = conn.createStatement();
            ru = st.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        closeConnection();
        return ru;
    }

    public ResultSet gselect(String sql) {
        // ResultSet rs = null;
        estConnection();
        try {
            st = conn.createStatement();
            rs = st.executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;

    }

    public static void main(String[] a) {
        DBMS d = new DBMS();
        d.estConnection();

    }
}
