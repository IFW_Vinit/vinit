/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.bean;

import java.util.Date;

/**
 *
 * @author heena
 */
public class Resolve_Complaint {
   private String emp_id;
   private String comp_id;
   private Date resolve_date;
   private String action_taken;

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    public Date getResolve_date() {
        return resolve_date;
    }

    public void setResolve_date(Date resolve_date) {
        this.resolve_date = resolve_date;
    }

    public String getAction_taken() {
        return action_taken;
    }

    public void setAction_taken(String action_taken) {
        this.action_taken = action_taken;
    }
   
   
}
