/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.servlet;

import com.ifw.bean.DBMS;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ifw.bean.*;
import java.util.ArrayList;

/**
 *
 * @author Raj
 */
@WebServlet(name = "PostManager", urlPatterns = {"/PostManager"})
public class PostManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostManager</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        ResultSet rs=null;
        int i = 0;
        DBMS db=new DBMS();
        List<Post> posts=new ArrayList<Post>();
        rs=db.gselect("select * from post_2");
        int size= 0;
        try
        {
if (rs != null)   
{  
              
                rs.beforeFirst();
            
  rs.last();  
  size = rs.getRow();  
}  
} catch (SQLException ex) {
                Logger.getLogger(PostManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        System.out.println("size of result set is="+size);
        Post PostObj[]=new Post[size];
        try {
            rs.beforeFirst();
        } catch (SQLException ex) {
            Logger.getLogger(PostManager.class.getName()).log(Level.SEVERE, null, ex);
        }
       posts.clear();
        try {
            while(rs.next())
            {
                PostObj[i]=new Post();
                PostObj[i].setEventID(rs.getString("eventId"));
                PostObj[i].setEventType(rs.getString("eventType"));
                PostObj[i].setFirstname(rs.getString("firstName"));
                PostObj[i].setLastname(rs.getString("lastName"));
                PostObj[i].setDept_name(rs.getString("deptName"));
                PostObj[i].setPos_name(rs.getString("positionName"));
                PostObj[i].setOrganizeFor(rs.getString("OrganizeFor"));
                PostObj[i].setSubject(rs.getString("subject"));
                PostObj[i].setDescription(rs.getString("description"));
                PostObj[i].setImage(rs.getString("image"));
                
                
                posts.add(PostObj[i]);
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Size of List="+posts.size());
        for(int j=0;j<=size-1;j++)
        {
            System.out.println(PostObj[j].getEventID());
            System.out.println(PostObj[j].getEventType());
            System.out.println(PostObj[j].getFirstname());
            System.out.println(PostObj[j].getLastname());
            System.out.println(PostObj[j].getDept_name());
            System.out.println(PostObj[j].getSubject());
            System.out.println(PostObj[j].getPos_name());
            System.out.println(PostObj[j].getDescription());
            System.out.println(PostObj[j].getImage());
            
            
            
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @thro
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * ws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
