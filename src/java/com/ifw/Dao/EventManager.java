/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Dao;

import com.ifw.bean.*;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vinit
 */
public class EventManager {
    
   private static final String VIEW_Department = "select * from department";	
   
		//---------method to view all Departments for Event  ----------//
		
		  public List<Department> viewAllDepartment()
		  {      
                          ResultSet rs = null;
			  List<Department> viewDepartmentObj = new ArrayList<Department>();
                          DBMS dbObj = new DBMS();
                          Department departmentObj = new Department();
			  try {
                                        rs = dbObj.gselect(VIEW_Department);
					while (rs.next()) {
					departmentObj.setDeptId(rs.getString("deptId"));
                                        departmentObj.setDeptName(rs.getString("deptName"));
			              
					viewDepartmentObj.add(departmentObj);
			                
			            }
					    
			} catch (Exception e) {
				
				e.printStackTrace();
			}
                          finally{
                           dbObj.closeConnection();
                          }
			  
			 return viewDepartmentObj; 
		  }
}
