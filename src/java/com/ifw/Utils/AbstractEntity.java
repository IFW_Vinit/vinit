/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifw.Utils;

import java.lang.reflect.Field;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author Pratik
 */
public class AbstractEntity {
     public void fillBean(HttpServletRequest request) {

        try {
            Class c = this.getClass();
            Field fields[] = c.getDeclaredFields();
            for (Field field : fields) {
                if (request.getParameter(field.getName()) != null) {
                    if (field.getType().getName().equals("int")) {
                        field.set(this, Integer.parseInt(request.getParameter(field.getName())));
                       
                    } else if (field.getType().getName().equals("java.lang.String")) {
                        request.getParameter(field.getName());
                        field.set(this, request.getParameter(field.getName()));
                    } else if (field.getType().getName().equals("java.lang.Integer")) {
                        field.set(this, Integer.parseInt(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("java.lang.Long")) {
                        field.set(this, Long.parseLong(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("java.util.Date")) {
                        java.util.Date d = new Date(request.getParameter(field.getName()));
                        java.sql.Date sql_date = new java.sql.Date(d.getTime());
                        field.set(this, sql_date);
                    } else if (field.getType().getName().equals("java.lang.Short")) {
                        field.set(this, Short.parseShort(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("java.lang.Double")) {
                        field.set(this, Double.parseDouble(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("java.lang.Boolean")) {
                        field.set(this, Boolean.parseBoolean(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("java.lang.Byte")) {
                        field.set(this, Byte.parseByte(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("Long")) {
                        field.set(this, Long.parseLong(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("double")) {
                        field.set(this, Double.parseDouble(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("boolean")) {
                        field.set(this, Boolean.parseBoolean(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("byte")) {
                        field.set(this, Byte.parseByte(request.getParameter(field.getName())));
                    } else if (field.getType().getName().equals("short")) {
                        field.set(this, Short.parseShort(request.getParameter(field.getName())));
                    }

//                else if (field.getType().getName().equals("date")) {
//                        field.set(this, Long.parseLong(request.getParameter(field.getName())));
//                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
