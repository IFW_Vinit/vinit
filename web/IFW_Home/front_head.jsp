<head>
<meta charset="utf-8">
<title>Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<script type="text/javascript" async="" src="assets/ga.js"></script><script async="" src="assets/cloudflare.min.js"></script>
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok2v=1613a3a185/"},atok:"fe4d94f90f4cdfba30552166aba7a4d1",petok:"3d23095fb93b6a2f0a937c0c3d66a86278226681-1420787292-1800",zone:"cesarlab.com",rocket:"0",apps:{"ga_key":{"ua":"UA-49490137-1","ga_bs":"2"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="../ajax.cloudflare.com/cdn-cgi/nexp/dok2v%3d919620257c/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link href="assets/css/social.core.css" rel="stylesheet">
<link href="assets/css/social.frontend.css" rel="stylesheet">
<link href="assets/css/font-awesome/font-awesome.css" rel="stylesheet">
 
<link id="current-theme" href="assets/css/themes/frontend/blue.css" rel="stylesheet">
 
<link href="assets/css/demo.css" rel="stylesheet">
<link href="assets/jquery.simplecolorpicker.css" rel="stylesheet">
 
<style>.wrapper .main{margin-top:60px;}</style>
<script src="assets/jquery.min.js"></script>
 
 <link rel="stylesheet" href="js/blueberry.css">
 
<!--[if lt IE 8]>
    <script src="assets/js/html5shiv/html5shiv.js"></script>
    <script src="assets/js/plugins/respond/respond.min.js"></script> 
    <![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-49490137-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.assets/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
/* ]]> */
</script><style type="text/css">.cf-hidden { display: none; } .cf-invisible { visibility: hidden; }</style>
</head>