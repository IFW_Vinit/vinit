<!--<html lang="en"><head>
<meta charset="utf-8">
-->

<!--
/*
 * jQuery Blueberry Slider v0.4 BETA
 * http://marktyrrell.com/labs/blueberry/
 *
 * Copyright (C) 2011, Mark Tyrrell <me@marktyrrell.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
-->

<link rel="stylesheet" href="js/blueberry.css">
<style type="text/css">
<!--
* {
	margin: 0;
	border: 0;
	padding: 0;
}
body {
	background: #f0f0f0;
	font: 14px/20px Arial, San-Serif;
	color: #404040;
}
a { color: #264c99; text-decoration: none; }
a:hover { text-decoration: underline; }

h1,h2,h3,h4,p { margin-bottom: 20px; }
h1 {
	font-size: 48px;
	line-height: 60px;
	color: #ffffff;
	text-transform: lowercase;
}
h2, h3 {
	font-weight: normal;
	font-size: 22px;
	line-height: 40px;
	color: #808080;
}
h3 { font-size: 18px; color: #404040; }
h5 { font-weight: bold; font-size: 14px; color: #000; }

#header {
	height: 60px;
	padding-top: 20px; padding-bottom: 20px;
	text-align: center;
	background: #405580;
}
#header h1 {
	margin: 0 auto;
	min-width: 740px;
	max-width: 1140px;
}
#doc { margin: 40px 0; }
#content {
	margin: 0 auto;
	min-width: 740px;
	max-width: 1140px;
}

.blueberry { max-width: 960px; }

-->
</style>
<style>
    .slide_height{
        
        height: 500px;
        
    }
    
</style>
<script src="assets/jquery.min.js"></script>
<script src="js/jquery.blueberry.js"></script>

<script>
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>

<!--</head>
<body>-->

<!--<div id="header">
  <h1><img src="http://marktyrrell.com/labs/blueberry/img/logo.png" alt="Blueberry"></h1>
</div>-->

<div id="doc">
    <div id="content" style="top:40px">

<!-- blueberry -->

    <div class="blueberry">
      <ul class="slides " >
        <li class="active" style="display: list-item;"><img class="slide_height"src="js/img1.jpg"></li>
        <li style="display: none;"><img class="slide_height" src="js/img2.jpg"></li>
        <li style="display: none;"><img class="slide_height"src="js/img3.jpg"></li>
        <li style="display: none;"><img class="slide_height"src="js/img4.jpg"></li>
      </ul>
    <ul class="pager"><li class="active"><a href="#"><span>0</span></a></li><li><a href="#"><span>1</span></a></li><li><a href="#"><span>2</span></a></li><li><a href="#"><span>3</span></a></li></ul></div>

<!-- blueberry -->

  </div>
</div>



<!--</body></html>-->