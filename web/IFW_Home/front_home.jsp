<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    
        <body><script type="text/javascript">
            //<![CDATA[
            try {
                (function(a) {
                    var b = "https:///", c = "cesarlab.com", d = "https://cesarlab.com/cdn-cgi/cl/", e = "img.html", f = new a;
                    f.src = [b, c, d, e].join("")
                })(Image)
            } catch (e) {
            }
            //]]>
            </script>
             <jsp:include page="front_head.jsp"></jsp:include>
            <div class="wrapper">

                <header>
                    <nav id="navbar" role="navigation" class="social-navbar navbar navbar-default navbar-fixed-top">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a href="#" class="navbar-brand">
                                    <h1>Social</h1>
                                </a>
                            </div>
                            <div class="navbar-collapse collapse">
                                <!--                            <ul class="nav navbar-nav navbar-right">
                                                                <li>
                                                                    <a href="index.html">Home</a>
                                                                </li>
                                                                <li class="dropdown">
                                                                    <a href="#" data-toggle="dropdown" data-hover="dropdown" data-delay="0" class="dropdown-toggle">Pages<b class="caret"></b>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li>
                                                                            <a href="pages_login.html">Log in</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_signup.html">Sign Up</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_about.html">About Us</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_blog_category.html">Blog Category</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_blog_page.html">Blog Page</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_pricing_tables.html">Pricing Tables</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_faq.html">FAQ</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_search_results.html">Search Results</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_error_404.html">404 Error Page</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="pages_error_500.html">500 Error Page</a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                                <li>
                                                                    <a href="features.html">Features</a>
                                                                </li>
                                                                <li>
                                                                    <a href="portfolio.html">Portfolio</a>
                                                                </li>
                                                                <li>
                                                                    <a href="contact.html">Contact</a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://cesarlab.com/templates/social/demo/admin/index.html">Admin</a>
                                                                </li>
                                                            </ul>-->
                            </div>

                        </div>
                    </nav>
                </header>

                <div class="main">

                    <!--<section id="banner" class="section-featured">
                    <div id="carousel-example-generic" data-ride="carousel" class="carousel slide">
                     
                    <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                    </ol>
                     
                    <div class="carousel-inner">
                    <div class="item active">
                    <div class="row header-text">
                    <div class="col-md-6 col-md-offset-3">
                    <div class="text-center"><i class="fa fa fa-thumbs-up fa-5x"></i>
                    <h2 class="slogan">Welcome to Social</h2>
                    <p>
                    Lorem ipsum dolor sit amet, natum bonorum expetendis usu ut. Eum impetus offendit disputationi eu, at vim aliquip lucilius praesent. Alia laudem antiopam te ius, sed ad munere integre, ubique facete sapientem nam ut.
                    </p>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div class="item">
                    <img src="assets/img/gallery/graph(1024x680).jpg" alt="Second slide">
                     
                    <div class="hidden-xs">
                    <div class="col-md-12 text-center">
                    <h2>
                    <span>Welcome to LOREM IPSUM</span>
                    </h2>
                    <br>
                    <h3>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                    </h3>
                    </div>
                    </div>
                     
                    </div>
                    <div class="item">
                    <img src="assets/img/gallery/beach_1200(1024x768).jpg" alt="Third slide">
                     
                    <div class="hidden-xs">
                    <div class="col-md-12 text-center">
                    <h2>
                    <span>Welcome to LOREM IPSUM</span>
                    </h2>
                    <br>
                    <h3>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                    </h3>
                    </div>
                    </div>
                     
                    </div>
                    </div>
                     
                    <a href="#carousel-example-generic" data-slide="prev" class="left carousel-control">
                    <span class="fa fa-chevron-left"></span>
                    </a>
                    <a href="#carousel-example-generic" data-slide="next" class="right carousel-control">
                    <span class="fa fa-chevron-right"></span>
                    </a>
                    </div>
                     
                    </section>-->
                    <section class="section section-services">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4" style="width: 40%"><i class="fa fa-code fa-5x"></i>
                                    <div style="height:40px"></div>
                                    <div class="login-panel panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Please Sign In</h3>
                                        </div>
                                        <div class="panel-body">
                                            <form role="form">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus="">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                                        </label>
                                                    </div>
                                                    <!-- Change this to a button or input when using this as a form -->
                                                    <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5" style="width: 60%"><i class="fa fa-terminal fa-5x"></i>
                                    <!--                                <h4>Responsive</h4>
                                                                    <p>
                                                                        Lorem ipsum dolor sit amet, mutat graeco volumus ad eam, singulis patrioque comprehensam nam no. Mei cu dicat voluptaria volumus.
                                                                    </p>-->
                                <jsp:include page="front_slider.jsp"></jsp:include>
                            </div>
                            <!--                            <div class="col-lg-4"><i class="fa fa-bolt fa-5x"></i>
                                                            <h4>Customizable</h4>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, mutat graeco volumus ad eam, singulis patrioque comprehensam nam no. Mei cu dicat voluptaria volumus.
                                                            </p>
                                                        </div>-->
                        </div>
                    </div>
                </section>

                <section class="section section-services">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4" style="width: 50%"><i class="fa fa-code fa-5x"></i>
                                <div style="height:50px"></div>
                                <div class="login-panel panel panel-default">
                                    <div class="panel-heading">
                                        <!--                        <h3 class="panel-title">Please Sign In</h3>-->
                                    </div>
                                    <div class="panel-body">
                                        <h1>hello</h1>
                                        <!--                        <form role="form">
                                                                    <fieldset>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="E-mail" name="email" autofocus="" type="email">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="Password" name="password" value="" type="password">
                                                                        </div>
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input name="remember" value="Remember Me" type="checkbox">Remember Me
                                                                            </label>
                                                                        </div>
                                                                         Change this to a button or input when using this as a form 
                                                                        <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>
                                                                    </fieldset>
                                                                </form>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4" style="width: 50%"><i class="fa fa-code fa-5x"></i>
                                <div style="height:50px"></div>
                                <div class="login-panel panel panel-default">
                                    <div class="panel-heading">
                                        <!--                        <h3 class="panel-title">Please Sign In</h3>-->
                                    </div>
                                    <div class="panel-body">
                                        <h1>hello</h1>
                                        <!--                        <form role="form">
                                                                    <fieldset>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="E-mail" name="email" autofocus="" type="email">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="Password" name="password" value="" type="password">
                                                                        </div>
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input name="remember" value="Remember Me" type="checkbox">Remember Me
                                                                            </label>
                                                                        </div>
                                                                         Change this to a button or input when using this as a form 
                                                                        <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>
                                                                    </fieldset>
                                                                </form>-->
                                    </div>
                                </div>
                            </div>
<!--                            <div class="col-lg-5" style="width: 50%"><i class="fa fa-terminal fa-5x"></i>
                            <div style="height:50px"></div>
                                <div class="login-panel panel panel-default">
                                    <div class="panel-heading">
                                                                <h3 class="panel-title">Please Sign In</h3>
                                    </div>
                            </div>
                                                        <div class="col-lg-4"><i class="fa fa-bolt fa-5x"></i>
                                                            <h4>Customizable</h4>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, mutat graeco volumus ad eam, singulis patrioque comprehensam nam no. Mei cu dicat voluptaria volumus.
                                                            </p>
                                                        </div>
                        </div>-->
                    </div>
                </section>
                <!--                <section class="section testimonials">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div>
                                                    <div class="testimonial">
                                                        <h5>Nunc velit risus, dapibus non interdum quis, suscipit nec dolor. Vivamus tempor tempus mauris vitae fermentum. In vitae nulla lacus. Sed sagittis tortor vel arcu sollicitudin nec tincidunt metus suscipit.Nunc velit risus, dapibus non interdum.</h5>
                                                        <br>
                                                        <span class="author">? MIKE DOE
                                                            <a href="#">www.siteurl.com</a>
                                                        </span>
                                                    </div>
                                               
                                              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>-->
                <section class="section section-about">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <div class="section-header">
                                    <h2 class="section-heading">Our Team</h2>
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet consectetur, adipisci velit, sed quia non numquam.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="team-member">
                                    <figure class="member-photo">
                                        <img src="assets/img/avatars/avatar.png" alt="Member Team">
                                    </figure>
                                    <div class="team-detail">
                                        <h4>Jason Doe</h4>
                                        <span>User experiences</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-member">
                                    <figure class="member-photo">
                                        <img src="assets/img/avatars/avatar.png" alt="Member Team">
                                    </figure>
                                    <div class="team-detail">
                                        <h4>Timothy Clark</h4>
                                        <span>Web developer</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="team-member">
                                    <figure class="member-photo">
                                        <img src="assets/img/avatars/avatar.png" alt="Member Team">
                                    </figure>
                                    <div class="team-detail">
                                        <h4>Vicky Tan</h4>
                                        <span>Web designer</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                  <div class="team-member">
                                      <figure class="member-photo">
                                              <img src="assets/img/avatars/avatar.png" alt="Member Team">
                                          </figure>
                                          <div class="team-detail">
                                              <h4>Kevin Peterson</h4>
                                              <span>UI designer</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </section>
                        <section class="section">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="quotes" class="section-quotes">
                                            <div id="quote-carousel" data-ride="carousel" class="carousel slide quotes-carousel">

                                            <ol class="carousel-indicators">
                                                <li data-target="#quote-carousel" data-slide-to="0" class=""></li>
                                                <li data-target="#quote-carousel" data-slide-to="1" class="active"></li>
                                                <li data-target="#quote-carousel" data-slide-to="2" class=""></li>
                                            </ol>

                                            <div class="carousel-inner">

                                                <div class="item">
                                                    <blockquote>
                                                        <div class="row">
                                                            <div class="col-sm-3 text-center">
                                                                <img src="assets/img/avatars/user1_55%402x.jpg" style="width: 100px;height:100px;" alt="User" class="img-circle">
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                                                                <small>Someone famous</small>
                                                            </div>
                                                        </div>
                                                    </blockquote>
                                                </div>

                                                <div class="item active">
                                                    <blockquote>
                                                        <div class="row">
                                                            <div class="col-sm-3 text-center">
                                                                <img src="assets/img/avatars/user2_55%402x.jpg" style="width: 100px;height:100px;" alt="User" class="img-circle">
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                                                <small>Someone famous</small>
                                                            </div>
                                                        </div>
                                                    </blockquote>
                                                </div>

                                                <div class="item">
                                                    <blockquote>
                                                        <div class="row">
                                                            <div class="col-sm-3 text-center">
                                                                <img src="assets/img/avatars/user3_55%402x.jpg" style="width: 100px;height:100px;" alt="User" class="img-circle">
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium.</p>
                                                                <small>Someone famous</small>
                                                            </div>
                                                        </div>
                                                    </blockquote>
                                                </div>
                                            </div>

                                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i>
                                            </a>
                                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>

                <footer class="section footer">
                    <div class="container">
                        <div class="row website-info">
                            <div class="col-sm-4 col-md-4">
                                <h4>Latest Posts</h4>
                                <ul class="posts-list">
                                    <li>
                                        <a href="#">Lorem ipsum dolor sit amet, consectetur.</a>
                                    </li>
                                    <li>
                                        <a href="#">Lorem ipsum.</a>
                                    </li>
                                    <li>
                                        <a href="#">Lorem ipsum dolor.</a>
                                    </li>
                                    <li>
                                        <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing.</a>
                                    </li>
                                    <li>
                                        <a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing.</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <h4>Social Networks</h4>
                                <div class="social-timeline">
                                    <dl class="dl-horizontal">
                                        <dt><i class="fa fa-twitter"></i>
                                        </dt>
                                        <dd>
                                            <a href="#ignore">@juliomrqz</a>&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, modi.&nbsp;
                                            <small>5 min ago</small>
                                        </dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt><i class="fa fa-facebook"></i>
                                        </dt>
                                        <dd>
                                            <a href="#ignore">Julio Marquez</a>&nbsp;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum, modi.&nbsp;
                                            <small>7 min ago</small>
                                        </dd>
                                    </dl>
                                </div>
                                <form action="#" class="form-inline">
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-inverse">Subscribe</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-4 col-md-4 company-info">
                                <h4>Who Are We?</h4>
                                <p>Lorem ipsum dolor slo onsec tueraliquet Morbi nec In Curabitur lreaoreet nisl lorem in pellente pellente eget. Lorem ipsum dolor slo onsec Vivamus.</p>
                                <address><strong>CESARLAB, Inc.</strong>
                                    <br>795 Street Ave, Suite 000
                                    <br>San Vegas, WT 9407
                                    <br><i class="fa fa-phone"></i>&nbsp;+1 (914) 820-6220</address><i class="fa fa-envelope"></i>&nbsp;support@cesarlab.com<script cf-hash="f9e31" type="text/javascript">
                                        /* <![CDATA[ */!function() {
                                            try {
                                                var t = "currentScript"in document ? document.currentScript : function() {
                                                    for (var t = document.getElementsByTagName("script"), e = t.length; e--; )
                                                        if (t[e].getAttribute("cf-hash"))
                                                            return t[e]
                                                }();
                                                if (t && t.previousSibling) {
                                                    var e, r, n, i, c = t.previousSibling, a = c.getAttribute("data-cfemail");
                                                    if (a) {
                                                        for (e = "", r = parseInt(a.substr(0, 2), 16), n = 2; a.length - n; n += 2)
                                                            i = parseInt(a.substr(n, 2), 16) ^ r, e += String.fromCharCode(i);
                                                        e = document.createTextNode(e), c.parentNode.replaceChild(e, c)
                                                    }
                                                }
                                            } catch (u) {
                                            }
                                        }();/* ]]> */</script>
                            </div>
                        </div>
                        <div class="row social-network-footer">
                            <div class="col-sm-12 align-center">
                                <ul>
                                    <li>
                                        <a title="RSS" class="btn btn-social-icon btn-warning"><i class="fa fa-rss"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Facebook" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Twitter" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Google+" class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a title="Linkedin" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row copyright">
                            <div class="col-sm-12">
                                <p>Copyright � 2014 Social - by&nbsp;
                                    <a href="http://cesarlab.com/" target="_blank">cesarlab.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

            <!--        <div class="theme-switcher">
                        <a href="#"><i class="fa fa-cogs"></i>
                        </a>
                        <div class="content"><strong>Color Style</strong>
                            <select name="colorpicker" class="styles" style="display: none;">
                                <option value="#242424" data-theme="assets/css/themes/frontend/default.css">Default</option>
                                <option value="#647AAB" data-theme="assets/css/themes/frontend/blue.css">Blue</option>
                                <option value="#ee5f5b" data-theme="assets/css/themes/frontend/red.css">Red</option>
                                <option value="#62c462" data-theme="assets/css/themes/frontend/green.css">Green</option>
                                <option value="#fbb450" data-theme="assets/css/themes/frontend/orange.css">Orange</option>
                            </select><span class="simplecolorpicker inline "><span class="color" title="Default" style="background-color: #242424;" data-color="#242424" data-selected="" role="button" tabindex="0"></span><span class="color" title="Blue" style="background-color: #647AAB;" data-color="#647AAB" role="button" tabindex="0"></span><span class="color" title="Red" style="background-color: #ee5f5b;" data-color="#ee5f5b" role="button" tabindex="0"></span><span class="color" title="Green" style="background-color: #62c462;" data-color="#62c462" role="button" tabindex="0"></span><span class="color" title="Orange" style="background-color: #fbb450;" data-color="#fbb450" role="button" tabindex="0"></span></span>
                            <br><strong>Top Navbar</strong>
                            <select name="navbar">
                                <option value="fixed">Fixed</option>
                                <option value="not-fixed">Not Fixed</option>
                            </select>
                            <hr>
                            <a href="https://cesarlab.com/templates/social/demo/frontend-rtl/index.html"><strong>RTL Version</strong>
                            </a>
                        </div>
                    </div>-->





            <!-- Mirrored from cesarlab.com/templates/social/demo/frontend/index.html by HTTrack Website Copier/3.x [XR&CO'2013], Fri, 09 Jan 2015 07:11:34 GMT -->
        </body>   
    </html>