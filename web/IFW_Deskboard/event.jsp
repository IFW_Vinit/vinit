<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>
        <link href="js/social.core.css" rel="stylesheet">
    
        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <!-- /.navbar-header -->

           
            <!-- /.navbar-top-links -->
            <jsp:include page="desk_menu.jsp"></jsp:include>
          

            <!-- 		</nav> -->

            <div style="min-height: 356px;" id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Event</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            
             
                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown">
                                            Actions <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <form novalidate="novalidate" id="advanced-form" class="form-horizontal">
                                <jsp:include page="Event_submenu.jsp"></jsp:include>
                                <div class="panel-body">
                                    <fieldset>

                                        <div class="form-group">
                                            <label for="title" class="col-md-4 control-label">Title</label>
                                            <div class="col-md-4">
                                                <select id="title" name="title" class="form-control">
                                                    <option value="1">Mr.</option>
                                                    <option value="2">Mrs.</option>
                                                    <option value="3">Miss.</option>
                                                    <option value="4">Ms.</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="firstname" class="col-md-4 control-label">First Name</label>
                                            <div class="col-md-4">
                                                <input id="firstname" name="firstname" placeholder="Your Frist Name" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                            <div class="col-md-4">
                                                <input id="lastname" name="lastname" placeholder="Your Last Name" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="email" class="col-md-4 control-label">Email</label>
                                            <div class="col-md-4">
                                                <input id="email" name="email" placeholder="Enter a valid email address" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="password1" class="col-md-4 control-label">Password</label>
                                            <div class="col-md-4">
                                                <input id="password1" name="password1" placeholder="Your Password" class="form-control input-md" type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="password2" class="col-md-4 control-label">Repeat Password</label>
                                            <div class="col-md-4">
                                                <input id="password2" name="password2" placeholder="Your Password Again" class="form-control input-md" type="password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="Mobile" class="col-md-4 control-label">Mobile</label>
                                            <div class="col-md-4">
                                                <input id="Mobile" name="Mobile" placeholder="Enter a valid Mobile Number" class="form-control input-md1" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="birthday" class="col-md-4 control-label">Birthday</label>
                                            <div class="col-md-4">
                                                <input id="birthday" name="birthday" placeholder="Enter a valid date" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="Country" class="col-md-4 control-label">Country</label>
                                            <div class="col-md-4">
                                                <input id="Country" name="Country" placeholder="Enter your country" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Gender</label>
                                            <div class="col-md-4">
                                                <div class="radio">
                                                    <label for="gender-0">
                                                        <input id="gender-0" name="gender" value="1" checked="checked" type="radio">Male
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label for="gender-1">
                                                        <input id="gender-1" name="gender" value="2" type="radio">Female
                                                    </label>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4">
                                                <label for="agree" class="checkbox-inline">
                                                    <input id="agree" name="terms" value="1" type="checkbox">I agree
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="button" class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Bar Chart Example
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown">
                                            Actions <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-clock-o fa-fw"></i> Responsive Timeline
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bell fa-fw"></i> Notifications Panel
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item"> <i
                                            class="fa fa-comment fa-fw"></i> New Comment <span
                                            class="pull-right text-muted small"><em>4 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-twitter fa-fw"></i> 3 New Followers <span
                                            class="pull-right text-muted small"><em>12 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-envelope fa-fw"></i> Message Sent <span
                                            class="pull-right text-muted small"><em>27 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-tasks fa-fw"></i> New Task <span
                                            class="pull-right text-muted small"><em>43 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-upload fa-fw"></i> Server Rebooted <span
                                            class="pull-right text-muted small"><em>11:32 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-bolt fa-fw"></i> Server Crashed! <span
                                            class="pull-right text-muted small"><em>11:13 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-warning fa-fw"></i> Server Not Responding <span
                                            class="pull-right text-muted small"><em>10:57 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-shopping-cart fa-fw"></i> New Order Placed <span
                                            class="pull-right text-muted small"><em>9:49 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-money fa-fw"></i> Payment Received <span
                                            class="pull-right text-muted small"><em>Yesterday</em> </span>
                                    </a>
                                </div>
                                <!-- /.list-group -->
                                <a href="#" class="btn btn-default btn-block">View All Alerts</a>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                      
                        <!-- /.panel -->
                        <div class="chat-panel panel panel-default">
                            <div class="panel-heading" id="chathead">
                                <i class="fa fa-comments fa-fw"></i> Chat
                                <div class="btn-group pull-right">
                                    <button type="button"
                                            class="btn btn-default btn-xs dropdown-toggle"
                                            data-toggle="dropdown">
                                        <i class="fa fa-chevron-down"></i>
                                    </button>
                                    <ul class="dropdown-menu slidedown">
                                        <li><a href="#"> <i class="fa fa-refresh fa-fw"></i>
                                                Refresh
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-check-circle fa-fw"></i>
                                                Available
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-times fa-fw"></i>
                                                Busy
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-clock-o fa-fw"></i>
                                                Away
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"> <i class="fa fa-sign-out fa-fw"></i>
                                                Sign Out
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body" id="chatbody">
                                <ul class="chat">
                                    <li class="left clearfix"><span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar"
                                                 class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">Jack Sparrow</strong> <small
                                                    class="pull-right text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 12 mins ago
                                                </small>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="right clearfix"><span
                                            class="chat-img pull-right"> <img
                                                src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                                class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 13 mins ago
                                                </small> <strong class="pull-right primary-font">Bhaumik
                                                    Patel</strong>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="left clearfix"><span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar"
                                                 class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">Jack Sparrow</strong> <small
                                                    class="pull-right text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 14 mins ago
                                                </small>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="right clearfix"><span
                                            class="chat-img pull-right"> <img
                                                src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                                class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 15 mins ago
                                                </small> <strong class="pull-right primary-font">Bhaumik
                                                    Patel</strong>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                </ul>
                            </div>
                            <!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="input-group">
                                    <input id="btn-input" class="form-control input-sm"
                                           placeholder="Type your message here..." type="text"> <span
                                           class="input-group-btn">
                                        <button class="btn btn-warning btn-sm" id="btn-chat">
                                            Send</button>
                                    </span>
                                </div>
                            </div>
                            <!-- /.panel-footer -->
                        </div>
                        <!-- /.panel .chat-panel -->
                    </div>
                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>