<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>

        <script type="text/javascript">
            $(document).ready(function () {
                alert("load");
                /* $("#chathead").click(function() {
                 
                 }); */
            });

        </script>
        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <jsp:include page="desk_menu.jsp"></jsp:include>

            <div style="min-height: 356px;" id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Complaint</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                            <form action="#" class="form-horizontal">
                                    <!-- BEGIN FORM-->
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-reorder"></i>Resole Complaint
                                            </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->

                                            <div class="form-body">


                                                <hr>
                                                <div class="form-group">
                                                    <label for="resolverid" class="col-md-4 control-label">Resolver Id</label>
                                                    <div class="col-md-4">
                                                        <hidden id="issue" name="resolver" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="issue" class="col-md-4 control-label">Complaint Id</label>
                                                    <div class="col-md-4">
                                                        <hidden id="issue" name="compid" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="action" class="col-md-4 control-label">Action Taken</label>
                                                    <div class="col-md-4">
                                                        <textarea class="form-control" rows="3"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="preventing" class="col-md-4 control-label">Preventing Measure</label>
                                                    <div class="col-md-4">
                                                        <textarea class="form-control" rows="3"></textarea>
                                                    </div>
                                                </div>
                                                
                                                <div class="panel-footer">
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-3 col-sm-9">
                                                            <button type="submit" class="btn btn-primary">Resolve</button>
                                                            <button type="button" class="btn btn-danger">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>										</form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>

                                <!--</div>
                                <!-- /.panel-body -->
                        
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bell fa-fw"></i> Notifications Panel
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item"> <i
                                            class="fa fa-comment fa-fw"></i> New Comment <span
                                            class="pull-right text-muted small"><em>4 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-twitter fa-fw"></i> 3 New Followers <span
                                            class="pull-right text-muted small"><em>12 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-envelope fa-fw"></i> Message Sent <span
                                            class="pull-right text-muted small"><em>27 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-tasks fa-fw"></i> New Task <span
                                            class="pull-right text-muted small"><em>43 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-upload fa-fw"></i> Server Rebooted <span
                                            class="pull-right text-muted small"><em>11:32 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-bolt fa-fw"></i> Server Crashed! <span
                                            class="pull-right text-muted small"><em>11:13 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-warning fa-fw"></i> Server Not Responding <span
                                            class="pull-right text-muted small"><em>10:57 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-shopping-cart fa-fw"></i> New Order Placed <span
                                            class="pull-right text-muted small"><em>9:49 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-money fa-fw"></i> Payment Received <span
                                            class="pull-right text-muted small"><em>Yesterday</em> </span>
                                    </a>
                                </div>
                                <!-- /.list-group -->
                                <a href="#" class="btn btn-default btn-block">View All Alerts</a>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="chat-panel panel panel-default">
                            <div class="panel-heading" id="chathead">
                                <i class="fa fa-comments fa-fw"></i> Chat
                                <div class="btn-group pull-right">
                                    <button type="button"
                                            class="btn btn-default btn-xs dropdown-toggle"
                                            data-toggle="dropdown">
                                        <i class="fa fa-chevron-down"></i>
                                    </button>
                                    <ul class="dropdown-menu slidedown">
                                        <li><a href="#"> <i class="fa fa-refresh fa-fw"></i>
                                                Refresh
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-check-circle fa-fw"></i>
                                                Available
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-times fa-fw"></i>
                                                Busy
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-clock-o fa-fw"></i>
                                                Away
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"> <i class="fa fa-sign-out fa-fw"></i>
                                                Sign Out
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body" id="chatbody">
                                <ul class="chat">
                                    <li class="left clearfix"><span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar"
                                                 class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">Jack Sparrow</strong> <small
                                                    class="pull-right text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 12 mins ago
                                                </small>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="right clearfix"><span
                                            class="chat-img pull-right"> <img
                                                src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                                class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 13 mins ago
                                                </small> <strong class="pull-right primary-font">Bhaumik
                                                    Patel</strong>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="left clearfix"><span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar"
                                                 class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">Jack Sparrow</strong> <small
                                                    class="pull-right text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 14 mins ago
                                                </small>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="right clearfix"><span
                                            class="chat-img pull-right"> <img
                                                src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                                class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 15 mins ago
                                                </small> <strong class="pull-right primary-font">Bhaumik
                                                    Patel</strong>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                </ul>
                            </div>
                            <!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="input-group">
                                    <input id="btn-input" class="form-control input-sm"
                                           placeholder="Type your message here..." type="text"> <span
                                           class="input-group-btn">
                                        <button class="btn btn-warning btn-sm" id="btn-chat">
                                            Send</button>
                                    </span>
                                </div>
                            </div>
                            <!-- /.panel-footer -->
                        </div>
                        <!-- /.panel .chat-panel -->
                    </div>
                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>