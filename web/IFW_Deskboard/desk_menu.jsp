	<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse"
				style="top: 42px; left: 0px; position: fixed; width: 250px">
				<ul class="nav in" id="side-menu">
					<li class="sidebar-search">
						<div class="input-group custom-search-form">
							<input class="form-control" placeholder="Search..." type="text">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button">
									<i class="fa fa-search"></i>
								</button>
							</span>
						</div> <!-- /input-group -->
					</li>
					<li><a class="active" href="desk_home.jsp"><i
							class="fa fa-dashboard fa-fw"></i> Home</a></li>
					<li><a href="desk_profile.jsp"><i class="fa fa-bar-chart-o fa-fw"></i>
							Profile<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li><a href="flot.html">Flot Charts</a></li>
							<li><a href="morris.html">Morris.js Charts</a></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="desk_event.jsp"><i class="fa fa-table fa-fw"></i>
							Event</a></li>
					<li><a href="desk_query.jsp"><i class="fa fa-edit fa-fw"></i>
							Forum</a></li>
					<li><a href="desk_complain.jsp"><i class="fa fa-wrench fa-fw"></i>
					        Complaint<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li><a href="panels-wells.html">Panels and Wells</a></li>
							<li><a href="buttons.html">Buttons</a></li>
							<li><a href="notifications.html">Notifications</a></li>
							<li><a href="typography.html">Typography</a></li>
							<li><a href="icons.html"> Icons</a></li>
							<li><a href="grid.html">Grid</a></li>
						</ul> <!-- /.nav-second-level --></li>
						
				    <li><a href="forms.html"><i class="fa fa-edit fa-fw"></i>
							Chat</a></li>
					<li><a href="#"><i class="fa fa-sitemap fa-fw"></i>
							Multi-Level Dropdown<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li><a href="#">Second Level Item</a></li>
							<li><a href="#">Second Level Item</a></li>
							<li><a href="#">Third Level <span class="fa arrow"></span></a>
								<ul class="nav nav-third-level collapse">
									<li><a href="#">Third Level Item</a></li>
									<li><a href="#">Third Level Item</a></li>
									<li><a href="#">Third Level Item</a></li>
									<li><a href="#">Third Level Item</a></li>
								</ul> <!-- /.nav-third-level --></li>
						</ul> <!-- /.nav-second-level --></li>
					<li><a href="#"><i class="fa fa-files-o fa-fw"></i> Sample
							Pages<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li><a href="blank.html">Blank Page</a></li>
							<li><a href="login.html">Login Page</a></li>
						</ul> <!-- /.nav-second-level --></li>
				</ul>
			</div>
			<!-- /.sidebar-collapse -->
		</div>