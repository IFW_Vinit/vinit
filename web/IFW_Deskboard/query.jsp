<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>

        <script type="text/javascript">
            $(document).ready(function () {
                alert("load");
                /* $("#chathead").click(function() {
                 
                 }); */
            });

        </script>
        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <!-- /.navbar-header -->

           
            <!-- /.navbar-top-links -->
            <jsp:include page="desk_menu.jsp"></jsp:include>
               
                <!-- /.navbar-static-side -->


                <!-- 		</nav> -->

                <div style="min-height: 356px;" id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Query</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-default">

                                <!-- /.panel-heading -->
                                <form action="../" method="POST" novalidate id="advanced-form" class="form-horizontal">
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                          
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->

                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label for="employee_name" class="col-md-4 control-label">Employee Name :-</label>
                                                    <div class="col-md-4">
                                                        <input id="employeeName_id" name="employee_name" class="form-control input-md" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="department" class="col-md-4 control-label">Department :-</label>
                                                    <div class="col-md-4">
                                                        <input id="department_id" name="department" class="form-control input-md" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="subject" class="col-md-4 control-label">Subject :-</label>
                                                    <div class="col-md-4">
                                                        <input id="subject_id" name="subject" class="form-control input-md" type="text">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="description" class="col-md-4 control-label">Description :-</label>
                                                    <div class="col-md-4">
                                                        <input id="description_id" name="description" class="form-control input-md1" type="text">
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-3 col-sm-9">
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                            <button type="button" class="btn btn-danger">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>



                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </form>



                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel --><!-- /.panel -->
                           
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-8 -->
                    <jsp:include page="desk_NotificationPanel.jsp"></jsp:include>
                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>