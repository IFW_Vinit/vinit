<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>

        <script type="text/javascript">
            $(document).ready(function () {
                alert("load");
                /* $("#chathead").click(function() {
                 
                 }); */
            });

        </script>
        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <jsp:include page="desk_menu.jsp"></jsp:include>

            <div style="min-height: 356px;" id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Complaint View</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8">
                        <form action="#" class="form-horizontal">
                            <!-- BEGIN FORM-->
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-globe"></i>Managed Table
                                            </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid"><div class="row"><div class="col-md-6 col-sm-12"></div><div class="col-md-6 col-sm-12"><div id="sample_1_filter" class="dataTables_filter"><label>Search: <input class="form-control input-medium" aria-controls="sample_1" type="text"></label></div></div></div><div class="table-scrollable"><table aria-describedby="sample_1_info" class="table table-striped table-bordered table-hover dataTable" id="sample_1">
                                                        <thead>
                                                            <tr role="row"><th aria-label="
                                                                         Email
                                                                         " style="width: 50px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    #
                                                                </th><th aria-label="
                                                                         Email
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Employee Id
                                                                </th><th aria-label="
                                                                         Joined
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Complaint Id
                                                                </th><th aria-label="
                                                                         Joined
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Complaint Date
                                                                </th><th aria-label="
                                                                         Joined
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Department Name
                                                                </th><th aria-label="
                                                                         Joined
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Subject
                                                                </th><th aria-label="
                                                                         Joined
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Description
                                                                </th><th aria-label="
                                                                         Joined
                                                                         " style="width: 158px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                                                                    Status
                                                                </th>
                                                           </thead>

                                                        <tbody aria-relevant="all" aria-live="polite" role="alert"><tr class="gradeX odd">
                                                                <td class=" ">
                                                                    1
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                                <td class=" ">
                                                                   
                                                                </td>
                                                                <td class="center ">
                                                                  </td>
                                                                <td class=" ">
                                                                    <span class="label label-sm label-success">
                                                                        Resolve
                                                                    </span>
                                                                </td>
                                                                <td class="center "><a href="#">Resolve</a>
                                                                  </td>
                                                                <td class="center "><a href="#">Reject</a>
                                                                  </td>
                                                            </tr><tr class="odd gradeX">
                                                               
                                                                
                                                                <td class=" ">
                                                                    2
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                                <td class=" ">
                                                                   
                                                                </td>
                                                                <td class="center ">
                                                                  </td>
                                                                <td class=" ">
                                                                    <span class="label label-sm label-success">
                                                                        Resolve
                                                                    </span>
                                                                </td>
                                                                
                                                                <td class="center "><a href="complaint_resolve.jsp">Resolve</a>
                                                                  </td>
                                                                <td class="center "><a href="complaint_reject.jsp">Reject</a>
                                                                  </td>
                                                            </tr><tr class="odd gradeX even">
                                                                
                                                                
                                                                <td class=" ">
                                                                    3
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                               <td class=" ">
                                                                    
                                                                </td>
                                                                <td class=" ">
                                                                   
                                                                </td>
                                                                <td class="center ">
                                                                  </td>
                                                                <td class=" ">
                                                                    <span class="label label-sm label-warning">
                                                                        Resolve
                                                                    </span>
                                                                </td>
                                                                
                                                                <td class="center "><a href="#">Resolve</a>
                                                                  </td>
                                                                <td class="center "><a href="#">Reject</a>
                                                                  </td>
                                                            </tr></tr>
                                                        
                                                        
                                                        
                                                        </tbody></table></div><div class="row"><div class="col-md-5 col-sm-12"><div id="sample_1_info" class="dataTables_info">Showing 1 to 5 of 25 entries</div></div><div class="col-md-7 col-sm-12"><div class="dataTables_paginate paging_bootstrap"><ul style="visibility: visible;" class="pagination"><li class="prev disabled"><a href="#" title="Prev"><i class="fa fa-angle-left"></i></a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li></ul></div></div></div></div>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>



                            <!--</div>
                            <!-- /.panel-body -->

                            <!-- /.panel -->

                    </div>


                    <!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bell fa-fw"></i> Notifications Panel
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item"> <i
                                            class="fa fa-comment fa-fw"></i> New Comment <span
                                            class="pull-right text-muted small"><em>4 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-twitter fa-fw"></i> 3 New Followers <span
                                            class="pull-right text-muted small"><em>12 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-envelope fa-fw"></i> Message Sent <span
                                            class="pull-right text-muted small"><em>27 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-tasks fa-fw"></i> New Task <span
                                            class="pull-right text-muted small"><em>43 minutes
                                                ago</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-upload fa-fw"></i> Server Rebooted <span
                                            class="pull-right text-muted small"><em>11:32 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-bolt fa-fw"></i> Server Crashed! <span
                                            class="pull-right text-muted small"><em>11:13 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-warning fa-fw"></i> Server Not Responding <span
                                            class="pull-right text-muted small"><em>10:57 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-shopping-cart fa-fw"></i> New Order Placed <span
                                            class="pull-right text-muted small"><em>9:49 AM</em> </span>
                                    </a> <a href="#" class="list-group-item"> <i
                                            class="fa fa-money fa-fw"></i> Payment Received <span
                                            class="pull-right text-muted small"><em>Yesterday</em> </span>
                                    </a>
                                </div>
                                <!-- /.list-group -->
                                <a href="#" class="btn btn-default btn-block">View All Alerts</a>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="chat-panel panel panel-default">
                            <div class="panel-heading" id="chathead">
                                <i class="fa fa-comments fa-fw"></i> Chat
                                <div class="btn-group pull-right">
                                    <button type="button"
                                            class="btn btn-default btn-xs dropdown-toggle"
                                            data-toggle="dropdown">
                                        <i class="fa fa-chevron-down"></i>
                                    </button>
                                    <ul class="dropdown-menu slidedown">
                                        <li><a href="#"> <i class="fa fa-refresh fa-fw"></i>
                                                Refresh
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-check-circle fa-fw"></i>
                                                Available
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-times fa-fw"></i>
                                                Busy
                                            </a></li>
                                        <li><a href="#"> <i class="fa fa-clock-o fa-fw"></i>
                                                Away
                                            </a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"> <i class="fa fa-sign-out fa-fw"></i>
                                                Sign Out
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body" id="chatbody">
                                <ul class="chat">
                                    <li class="left clearfix"><span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar"
                                                 class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">Jack Sparrow</strong> <small
                                                    class="pull-right text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 12 mins ago
                                                </small>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="right clearfix"><span
                                            class="chat-img pull-right"> <img
                                                src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                                class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 13 mins ago
                                                </small> <strong class="pull-right primary-font">Bhaumik
                                                    Patel</strong>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="left clearfix"><span class="chat-img pull-left">
                                            <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar"
                                                 class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <strong class="primary-font">Jack Sparrow</strong> <small
                                                    class="pull-right text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 14 mins ago
                                                </small>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                    <li class="right clearfix"><span
                                            class="chat-img pull-right"> <img
                                                src="http://placehold.it/50/FA6F57/fff" alt="User Avatar"
                                                class="img-circle">
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"> <i
                                                        class="fa fa-clock-o fa-fw"></i> 15 mins ago
                                                </small> <strong class="pull-right primary-font">Bhaumik
                                                    Patel</strong>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing
                                                elit. Curabitur bibendum ornare dolor, quis ullamcorper
                                                ligula sodales.</p>
                                        </div></li>
                                </ul>
                            </div>
                            <!-- /.panel-body -->
                            <div class="panel-footer">
                                <div class="input-group">
                                    <input id="btn-input" class="form-control input-sm"
                                           placeholder="Type your message here..." type="text"> <span
                                           class="input-group-btn">
                                        <button class="btn btn-warning btn-sm" id="btn-chat">
                                            Send</button>
                                    </span>
                                </div>
                            </div>
                            <!-- /.panel-footer -->
                        </div>
                        <!-- /.panel .chat-panel -->
                    </div>
                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>