<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <jsp:include page="desk_head.jsp"></jsp:include>
    <jsp:include page="desk_script.jsp"></jsp:include>
        <link href="js/social.core.css" rel="stylesheet">

        <body>
        <%--<jsp:include page="../header.jsp"></jsp:include>--%>
        <div id="wrapper">

            <!-- Navigation -->
            <!--  <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">  -->

            <div class="navbar-header1">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <!--   <a class="navbar-brand" href="index.html">SB Admin v2.0</a> -->
            </div>
            <!-- /.navbar-header -->


            <!-- /.navbar-top-links -->
            <jsp:include page="desk_menu.jsp"></jsp:include>


                <!-- 		</nav> -->

                <div style="min-height: 356px;" id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Event</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>


                    <div class="row">
                        <div class="col-lg-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                                    <div class="pull-right">
                                        <div class="btn-group">
                                            <button type="button"
                                                    class="btn btn-default btn-xs dropdown-toggle"
                                                    data-toggle="dropdown">
                                                Actions <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <form novalidate="novalidate" id="advanced-form" class="form-horizontal">
                                <jsp:include page="Event_submenu.jsp"></jsp:include>
                                <div class="panel-body">
                                    <fieldset>


                                        <div class="form-group">
                                            <label for="firstname" class="col-md-4 control-label">Organized For :-</label>
                                            <div class="col-md-4">
                                                <input id="firstname" name="firstname" placeholder="Your Frist Name" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="type" class="col-md-4 control-label">Type :-</label>
                                            <div class="col-md-4">
                                                <select id="type" name="type" class="form-control input-md">
                                                    <option class="form-control input-md" selected>Select Type of Event</option>
                                                    <option class="form-control input-md">Award</option>
                                                     <option class="form-control input-md">Conference</option>
                                                    <option class="form-control input-md">Tour</option>
                                                    <option class="form-control input-md">Party</option>
                                                    <option class="form-control input-md">Festival</option>
                                                    <option class="form-control input-md">Other</option>
                                                </select>
                                                
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="subject" class="col-md-4 control-label">Subject :-</label>
                                            <div class="col-md-4">
                                                <input id="subject" name="subject" placeholder="Enter Subject of Event" class="form-control input-md" type="text">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="description" class="col-md-4 control-label">Description :-</label>
                                            <div class="col-md-4">
                                                <textarea cols="25" rows="5" id="description" name="description" placeholder="Event short Description" class="form-control input-md" style="width: 250px;"></textarea>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="eventImage" class="col-md-4 control-label">Image :-</label>
                                            <div class="col-md-4">
                                                <input id="eventImage" name="eventImage" placeholder="Your Password Again" type="file">
                                            </div>
                                        </div>


                                    </fieldset>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary">Post</button>
                                            <button type="button" class="btn btn-danger">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Bar Chart Example
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button"
                                                class="btn btn-default btn-xs dropdown-toggle"
                                                data-toggle="dropdown">
                                            Actions <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-clock-o fa-fw"></i> Responsive Timeline
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                                 <jsp:include page="desk_NotificationPanel.jsp"></jsp:include>
                    <!-- /.col-lg-8 -->
                   
                    <!-- /.col-lg-4 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


    </body>
</html>