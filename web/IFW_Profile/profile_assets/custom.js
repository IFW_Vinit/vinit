var $j = jQuery.noConflict();
/* ---------------------------------------------------- */
/*	MOBILE MENU 										*/
/* ---------------------------------------------------- */
jQuery(document).ready(function(){
	'use strict';
	jQuery('#menu-main').superfish();
	jQuery('#menu-main li.current-menu-item,#menu-main li.current_page_item,#menu-main li.current-menu-parent,#menu-main li.current-menu-ancestor,#menu-main li.current_page_ancestor').each(function(){
		jQuery(this).prepend('<span class="item_selected"></span>');
	});
});

(function( $ ) {
	'use strict';
	$.fn.sktmobilemenu = function( options ) { 
		var defaults = {
			'fwidth': 1025
		};
		//call in the default otions
		var options = $.extend(defaults, options);
		var obj = $(this);
		
		return this.each(function() {
			if($(window).width() < options.fwidth) {
				sktMobileRes();
			}
			
			$(window).resize(function() {
				if($(window).width() < options.fwidth) {
					sktMobileRes();
				}else{
					sktDeskRes();
				}
			});
			function sktMobileRes() {
				jQuery('#menu-main').superfish('destroy');
				obj.addClass('skt-mob-menu').hide();
				obj.parent().css('position','relative');
				if(obj.prev('.sktmenu-toggle').length === 0) {
					obj.before('<div class="sktmenu-toggle" id="responsive-nav-button"></div>');
				}
				obj.parent().find('.sktmenu-toggle').removeClass('active');
			}
			function sktDeskRes() {
				jQuery('#menu-main').superfish('init');
				obj.removeClass('skt-mob-menu').show();
				if(obj.prev('.sktmenu-toggle').length) {
					obj.prev('.sktmenu-toggle').remove();
				}
			}
			obj.parent().on('click','.sktmenu-toggle',function() {
				if(!$(this).hasClass('active')){
					$(this).addClass('active');
					$(this).next('ul').stop(true,true).slideDown();
				}
				else{
					$(this).removeClass('active');
					$(this).next('ul').stop(true,true).slideUp();
				}
			});
		});
	};
})( jQuery );

jQuery(window).load(function(){
   jQuery('#menu-main.skt-mob-menu').on('click','li.menu-item-has-children',function() {
		if(jQuery(this).hasClass('active')){
			jQuery(this).removeClass('active');
			jQuery(this).next('ul:first').stop(true,true).slideUp();
		}
		else{
			jQuery(this).addClass('active');
			jQuery(this).next('ul:first').stop(true,true).slideDown();
		}
	});
});

jQuery(document).ready(function ($) {
	'use strict';
	document.getElementById('s') && document.getElementById('s').focus();
});
jQuery(document).ready(function(){
	'use strict';
	jQuery( ".sf-with-ul" ).append( '<span class="caret"></span>' );
});

jQuery(window).load(function(){ 
	// if(jQuery('#skenav .skt-mob-menu').length > 0){		
		// jQuery('#skenav .skt-mob-menu').css('width',jQuery('.row-fluid').width());
	// }
});

/* ---------------------------------------------------- */
/*	BACK TO TOP 										*/
/* ---------------------------------------------------- */
jQuery(document).ready( function() {
	'use strict';
	jQuery('#back-to-top,#backtop').hide();
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > 100) {
			jQuery('#back-to-top,#backtop').fadeIn();
		} else {
			jQuery('#back-to-top,#backtop').fadeOut();
		}
	});
	jQuery('#back-to-top,#backtop').click(function(){
		jQuery('html, body').animate({scrollTop:0}, 'slow');
	});
});


jQuery(document).ready(function($) {
	'use strict';
	//SEARCH BOX
	jQuery('.search-strip, .hsearch .hsearch-close').on('click', function(){
		jQuery('.hsearch .row-fluid').fadeToggle( "fast", "linear" );
		jQuery('body').toggleClass('overflowhide');
	});
}); 

/* ---------------------------------------------------- */
/*	PRETTYPHOTO								            */
/* ---------------------------------------------------- */
jQuery(document).ready(function ($) {
	'use strict';
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
		hook: 'data-rel',
		animation_speed:'normal',
		theme:'light_square',
		slideshow:3000,
		show_title:false,
		autoplay_slideshow: false,
		social_tools: false		
	});
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({
		animation_speed:'normal',
		theme:'light_square',
		slideshow:3000,
		show_title:false,
		autoplay_slideshow: false,
		social_tools: false		
	});
});
//--------------------------------------------------------


/* ---------------------------------------------------- */
/*	CONTACT PAGE HEADER HEIGHT						    */
/* ---------------------------------------------------- */
jQuery(document).ready(function ($) {
	'use strict';
	jQuery('body.page-template-template-contact-page-php .header-top-wrap').height(jQuery(window).height());
	jQuery('body.page-template-template-contact-page-php .header-top-wrap').width(jQuery(window).width());
	
	jQuery('#contact-gmap-toggle').click(function(){
		jQuery('.header-contain-cover,.contact-map-overlay').fadeToggle( "fast", "easeInQuad" );
	});
});
//------------------------------------------------------


//ISOTOP JQUERY ----------------------------------------
//------------------------------------------------------

var $container = jQuery('#container-isotop');

jQuery(window).load( function() {
	if(jQuery(window).width() > 1199) {
		$container.masonry({
			columnWidth: 370,
			itemSelector: '.item',
			gutter:30
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() >= 1025 && jQuery(window).width() <= 1199) {
		$container.masonry({
			columnWidth: 326,
			itemSelector: '.item',
			gutter:12
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() >= 767 && jQuery(window).width() <= 1024) {
		$container.masonry({
			columnWidth: 354,
			itemSelector: '.item',
			gutter:26
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() >= 500 && jQuery(window).width() <= 766) {
		$container.masonry({
			columnWidth: 354,
			itemSelector: '.item',
			gutter:26
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() < 500) {
		$container.masonry({
			columnWidth: 291,
			itemSelector: '.item',
			gutter:20
		});
		$container.css({'visibility':'visible'});
	}
});


jQuery(window).resize(function(){

	if(jQuery(window).width() > 1199) {
		$container.masonry({
			columnWidth: 370,
			itemSelector: '.item',
			gutter:30
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() >= 1025 && jQuery(window).width() <= 1199) {
		$container.masonry({
			columnWidth: 326,
			itemSelector: '.item',
			gutter:12
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() >= 767 && jQuery(window).width() <= 1024) {
		$container.masonry({
			columnWidth: 354,
			itemSelector: '.item',
			gutter:26
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() >= 500 && jQuery(window).width() <= 766) {
		$container.masonry({
			columnWidth: 354,
			itemSelector: '.item',
			gutter:26
		});
		$container.css({'visibility':'visible'});
	}
	if(jQuery(window).width() < 500) {
		$container.masonry({
			columnWidth: 291,
			itemSelector: '.item',
			gutter:20
		});
		$container.css({'visibility':'visible'});
	}
})


jQuery(document).ready(function(){
	jQuery("#loading").hide();
	jQuery("#ap_no_posts").hide();
	
	var postsperpage = jQuery("#postsperpage").val();
	var skt_total_posts = jQuery("#skt_total_posts").val();
	var skt_nextposts = jQuery("#skt_nextposts").val();
	var appage_number = 1;
	var skt_runonce =  true;
	
	skt_timesloop = skt_total_posts % postsperpage;

	if(skt_timesloop == 0){
		skt_timesloop = (skt_total_posts / postsperpage) - 1;	
	}
	else{
		skt_timesloop = Math.floor(skt_total_posts / postsperpage);
	}
	
	if(jQuery('#full-twitter-box').length > 0){
		fullTwitterHt = jQuery('#full-twitter-box').height();
	}else{
		fullTwitterHt = 0;
	}
	
	if(jQuery('#footer').length > 0){
		footerHt = jQuery('#footer').height();
	}else{
		footerHt = 0;
	}
	
	var callready = true;
	
	jQuery(window).scroll(function() {
		if ( callready && jQuery(document).scrollTop() + jQuery(window).height() >= jQuery("body").height() - (fullTwitterHt + footerHt)){
		
			callready = false; //Set the flag here
			appage_number++;
			if(skt_timesloop){ 
				skt_timesloop--;
				jQuery.ajax({
					type: "POST",
					url: skt_nextposts,
					cache:false,
					beforeSend: function() { jQuery('#blog-masonry #skt_loading').fadeIn(200); },
					data: 'pagenumber='+appage_number+'&postsperpage='+postsperpage,
					success: function(data) { 
						jQuery('#blog-masonry #skt_loading').hide(); 
						$container.append(data); 
						$container.imagesLoaded( function() { $container.masonry('reloadItems').masonry(); }); 
						jQuery('#blog-masonry .image-gallery-slider').flexslider();
						callready = true;
					}
				});
			}
			else{
				jQuery('#blog-masonry #skt_loading').fadeIn(200,function(){jQuery('#blog-masonry #skt_loading').delay(200).hide();jQuery("#skt_noposts_load").delay(400).fadeIn(200) });
			}
		}
	});
});


//-----------------------------------------------------


/* ---------------------------------------------------- */
/*	CONTACT PAGE HEADER HEIGHT						    */
/* ---------------------------------------------------- */
function SktSubsWidgetValid(){
	var reg= /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var a  = document.getElementById('subs-widget-emailid').value;

	if( a == "Enter email address"){
		jQuery('#subs-widget-emailid').css({'border-color':'red','color':'red'});
		return false;
	}
	else{
		if(reg.test(a)==false){
			jQuery('#subs-widget-emailid').css({'border-color':'red','color':'red'});
			return false;
		}	
	}	
	return true;
}
//WAYPOINTS MAGIC -----------------------------------------
//---------------------------------------------------------
if ( typeof window['vc_waypoints'] !== 'function' ) {
function vc_waypoints() {
if (typeof jQuery.fn.waypoint !== 'undefined') {
$j('.fade_in_hide').waypoint(function() {
$j(this).addClass('skt_start_animation');
}, { offset: '90%' });
$j('.skt_animate_when_almost_visible').waypoint(function() {
$j(this).addClass('skt_start_animation');
}, { offset: '90%' });
}
}
}

jQuery(document).ready(function($) {
'use strict';
vc_waypoints();
jQuery('.skt-counter').waypoint(function() {

            var counter = jQuery(this).find('.skt-counter-number'),
                count = parseInt(counter.text(), 10),
                prefix = '',
                suffix = '',
                number = 0;

            if (jQuery(this).data('count')) {
                count = parseInt(jQuery(this).data('count'), 10);
            }
            if (jQuery(this).data('prefix')) {
                prefix = jQuery(this).data('prefix');
            }
            if (jQuery(this).data('suffix')) {
                suffix = jQuery(this).data('suffix');
            }

            var	step = Math.ceil(count/25),
                handler = setInterval(function() {
                    number += step;
                    counter.text(prefix+number+suffix);
                    if (number >= count) {
                        counter.text(prefix+count+suffix);
                        window.clearInterval(handler);
                    }
                }, 40);


 }, {offset:'85%', triggerOnce: true});
}); 