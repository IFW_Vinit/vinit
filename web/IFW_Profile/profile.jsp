<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<jsp:include page="profile_head.jsp"></jsp:include>
<jsp:include page="profile_script.jsp"></jsp:include>

<body class="home blog">

	<div id="wrapper" class="skepage">
	
		<div style="height: 85px;" class="header-top-wrap clearfix">
		
			<div class="header-contain-cover">
			
				<div style="top: 0px; display: block;" id="header" class="header-topbar skehead-headernav clearfix">
					<div class="container"> 
						<div class="row-fluid">
							<!-- #logo -->
							<div id="logo" class="span4">
																	<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo" title="Convac ? Multi Author Blogging Demo"><img class="logo" src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/logo.png" alt=""></a>
															</div>
							<!-- #logo -->
							
							<!-- .top-nav-menu --> 
							<div class="top-nav-menu span8"> 
								<div class="top-nav-menu span11">
									<div id="skenav" class="ske-menu"><ul id="menu-main" class="menu sf-js-enabled sf-arrows"><li id="menu-item-81" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-81"><a class="sf-with-ul" href="#">Home<span class="caret"></span></a>
<ul style="display: none;" class="sub-menu">
	<li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/home-left-sidebar/">Home + Left Sidebar</a></li>
	<li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/home-masonry/">Home + Masonry</a></li>
	<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/home-no-sidebar/">Home + No Sidebar</a></li>
</ul>
</li>
<li id="menu-item-36" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-36"><a class="sf-with-ul" href="#">Page<span class="caret"></span></a>
<ul style="display: none;" class="sub-menu">
	<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/page-left-sidebar/">Page + Left Sidebar</a></li>
	<li id="menu-item-34" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/page-no-sidebar/">Page + No Sidebar</a></li>
	<li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/page-right-sidebar/">Page + Right Sidebar</a></li>
	<li id="menu-item-162" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-162"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/page-404/">404 Page</a></li>
</ul>
</li>
<li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-69"><a class="sf-with-ul" href="#">Post Formats<span class="caret"></span></a>
<ul style="display: none;" class="sub-menu">
	<li id="menu-item-71" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-71"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/standard-post/">Standard Post</a></li>
	<li id="menu-item-72" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-72"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/gallery/">Gallery Post</a></li>
	<li id="menu-item-70" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-70"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/video/">Video Post</a></li>
	<li id="menu-item-73" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-73"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/quote/">Quote Post</a></li>
	<li id="menu-item-75" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-75"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/audio/">Audio Post</a></li>
	<li id="menu-item-76" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-76"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/sound-cloud/">Sound Cloud Post</a></li>
	<li id="menu-item-74" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-74"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/link/">Link Post</a></li>
</ul>
</li>
<li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/contact-us/">Contact Us</a></li>
<li id="menu-item-82" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-82"><a class="sf-with-ul" href="#">Shortcodes<span class="caret"></span></a>
<ul style="display: none;" class="sub-menu">
	<li id="menu-item-108" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-108"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/buttons-tooltips/">Buttons &amp; Tooltips</a></li>
	<li id="menu-item-107" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-107"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/blockquote-dropcaps/">Blockquote &amp; Dropcaps</a></li>
	<li id="menu-item-109" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-109"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/content-boxes/">Content Boxes</a></li>
	<li id="menu-item-110" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/custom-list-icons/">Custom List Icons</a></li>
	<li id="menu-item-111" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-111"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/googlemap-video/">GoogleMap &amp; Video</a></li>
	<li id="menu-item-112" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-112"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/pricing-table/">Pricing Table</a></li>
	<li id="menu-item-113" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-113"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/share-divider/">Share &amp; Divider</a></li>
	<li id="menu-item-114" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-114"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/statistics/">Statistics</a></li>
	<li id="menu-item-115" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tabs/">Tabs</a></li>
	<li id="menu-item-116" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/testimonials/">Testimonials</a></li>
	<li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/toggle-accordian/">Toggle &amp; Accordian</a></li>
	<li id="menu-item-118" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/typography/">Typography</a></li>
</ul>
</li>
</ul></div>								</div>
																	<li class="nav-search-icon span1">
										<a href="javascript:void(0);" class="search-strip" title="search"><i class="fa fa-search"></i></a>
										<!-- search-strip -->
										<div class="hsearch">
											<div class="container">
												<div class="row-fluid">
													<form method="get" id="header-searchform" action="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/">
														<fieldset>
															<input value="" placeholder="Search Here ..." id="s" name="s" type="text">
															<input value="Search" id="header-searchsubmit" type="submit">
														</fieldset>
													</form>
													<div class="hsearch-close"></div>
												</div>
											</div>
										</div>
										<!-- search-strip -->
									</li>
															</div>
							<!-- .top-nav-menu --> 
						</div>
					</div>					
				</div>
				<div style="height: 0px;" class="header-clone"></div>
				<!-- header-topbar -->
				
				
				<!-- Header Author Slider Starts -->
						<div class="container">
			<div style="visibility: visible;" class="row-fluid author-slider-wrap">
				<div style="margin-top: 21px; margin-bottom: 21px;" id="author-slider" class="flexslider">
					<ul class="slides">
														<li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
									<p style="left: 0px; opacity: 1;" class="flex-caption">
										<img style="width: 122px; height: 122px;" draggable="false" src="profile_assets/Profile-pic-03.png">										<span style="margin-top: 21px; font-size: 24px; line-height: 24px; padding-bottom: 9px;" class="slider-title">damien</span>
										<span style="height: 33px; font-size: 12px; line-height: 18px;" class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute .</span>									</p>
								</li>
															<li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
									<p style="left: 0px; opacity: 1;" class="flex-caption">
										<img style="width: 122px; height: 122px;" draggable="false" src="profile_assets/Profile-pic-04.png">										<span style="margin-top: 21px; font-size: 24px; line-height: 24px; padding-bottom: 9px;" class="slider-title">Susan</span>
										<span style="height: 33px; font-size: 12px; line-height: 18px;" class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sit amet massa sit amet odio interdum facilisis non nec dolor. Donec consequat urna ac nulla cursus, et consectetur elit congue. Nulla condimentum cursus porttitor.</span>									</p>
								</li>
															<li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
									<p style="left: 0px; opacity: 1;" class="flex-caption">
										<img style="width: 122px; height: 122px;" draggable="false" src="profile_assets/Profile-pic-02.png">										<span style="margin-top: 21px; font-size: 24px; line-height: 24px; padding-bottom: 9px;" class="slider-title">SARAH</span>
										<span style="height: 33px; font-size: 12px; line-height: 18px;" class="text">Inbound Marketing Consultant: SEM Guru - SEO Jedi - Content Marketing Wizard - Head Hunting Warrior. Entrepreneur, Wife &amp; Proud mother. I solve problems.</span>									</p>
								</li>
												</ul>
					<!-- .slides -->
				<ul class="flex-direction-nav"><li><a class="flex-prev" href="#">Previous</a></li><li><a class="flex-next" href="#">Next</a></li></ul></div>
				<!-- #author-slider ends -->
			</div>
			<!-- .row-fluid ends -->
		</div>
		<!-- .container ends -->
						<!-- Header Author Slider Ends -->
				
								
					<!-- SOCIAL LINKS SECTION -->
										<div style="visibility: visible; margin-bottom: 21px;" class="social_icon container">
						<div style="margin-top: 21px; font-size: 19px; line-height: 19px;" class="head_social_title">CONNECT US AT</div>						<ul style="margin-top: 21px;" class="clearfix">
							<li class="fb-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-facebook" title="Facebook"></span></a></li>							<li class="tw-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-twitter" title="Twitter"></span></a></li>							<li class="gplus-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-google-plus" title="Google Plus"></span></a></li>							<li class="pinterest-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-pinterest" title="Pinterest"></span></a></li>							<li class="linkedin-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-linkedin" title="Linkedin"></span></a></li>							<li class="foursquare-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-foursquare" title="Foursquare"></span></a></li>							<li class="flickr-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-flickr" title="Flickr"></span></a></li>							<li class="youtube-icon"><a style="font-size: 11px; line-height: 13px; width: 13px; height: 13px;" target="_blank" href="#"><span class="fa fa-youtube-play" title="Youtube"></span></a></li>						</ul>
					</div>
										<!-- SOCIAL LINKS SECTION -->
				
							
			</div>
			
			<!-- IF IS CONTACT PAGE TEMPLATE -->
						<!-- IF IS CONTACT PAGE TEMPLATE END -->
			
		</div>
	
	<!-- header image section -->

<div id="main" class="clearfix">
<div class="main-wrapper-item">
	<div class="bread-title-holder">
		<!-- <div class="bread-title-bg-image full-bg-breadimage-fixed"></div>
		 <div class="container">
			 <div class="row-fluid">
				  <div class="container_inner clearfix">
					 <h1 class="title">Blog</h1>
				   </div>
			 </div>
		</div> -->
	</div> 
<jsp:include page="../header.jsp"></jsp:include>
	<div class="page-content">
		<div class="container post-wrap">
			 <div class="row-fluid">
				  <div id="container" class="span8">
					<div id="content">
																								<div class="post-38 post type-post status-publish format-standard has-post-thumbnail hentry category-standard-post tag-standard" id="post-38">

		
	<h1 class="post-title">
		<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/" title="Standard Post">Standard Post</a>
	</h1>
	
	<div class="convac-post-box clearfix">
	
				<div class="featured-image-shadow-box">
						<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/" class="image"><img src="profile_assets/Slide2-636x441.jpg" alt="Standard Post" class="featured-image alignnon"></a>
		</div>
		
		<div class="skepost-meta clearfix">
			<span class="post-type-img"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/standard-post.png"></span>
			<span class="date"><span>18</span><br>Jul,2014</span>
			<span class="author-name"><img class="avatar photo" src="profile_assets/Profile-pic-03.png"></span>
			<span class="comments"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/comment-box.png"><br><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/#respond" title="Comment on Standard Post"><span>0</span></a></span>
			<span class="blog-post-like"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-meta-like.png"><br><span class="ul_cont" onclick="alter_ul_post_values(this,'38','_ldc_plc')"><span class="like_img ldc_img"></span> <span class="ldc_counts">22</span></span></span>
		</div><!-- skepost-meta -->
			</div><!--  -->

	
	<div class="skepost">
		<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut?</p>
 
		<div class="continue"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/">Read More</a></div>		  
	</div>
	<!-- skepost -->
	
	<div class="skt-blog-share clearfix">
		<ul>
			<!--Twitter-->
			<li class="twitter-icon"><a target="_blank" href="http://twitter.com/home?status=Standard Posthttp://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/"><i class="fa fa-twitter"></i></a></li>
			<!--Facebook-->
			<li class="facebook-icon"><a target="_blank" href="http://www.facebook.com/sharer.php?u=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/&amp;t=Standard Post"><i class="fa fa-facebook"></i></a></li>
			<!--Linked In-->
			<li class="linkedin-icon"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/&amp;title=Standard Post&amp;summary=&amp;source="><i class="fa fa-linkedin"></i></a></li>
			<!--Google Plus-->
			<li class="gplusicon-icon"><a target="_blank" href="https://plus.google.com/share?url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/"><i class="fa fa-google-plus"></i></a></li>
			<!-- PINTEREST -->
			<li class="pinterest-icon"><a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media=&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/&amp;is_video=false&amp;description=Standard Post"><i class="fa fa-pinterest"></i></a></li>
		</ul>
	</div><!-- skt-blog-share -->
	
	
</div>
<!-- post -->												
<div class="post-47 post type-post status-publish format-gallery hentry category-gallery tag-gallery tag-post" id="post-47">

	<h1 class="post-title">
		<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/" title="Gallery Post">Gallery Post</a>
	</h1>

		
	<div class="convac-post-box clearfix">
	
		<!-- GALLERY STARTS -->
		<div class="slider-attach">
				


			<div class="image-gallery-slider post-slider-47" id="post-slider-47">
				<ul class="gallery-box slides skt-post-slider-47"><li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> 
														<img draggable="false" src="profile_assets/picjumbo.com_IMG_5540-636x441.jpg" alt="47" height="441" width="636">
						</li><li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> 
														<img draggable="false" src="profile_assets/IMG_5243-636x441.jpg" alt="47" height="441" width="636">
						</li><li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;"> 
														<img draggable="false" src="profile_assets/9172931230_01425f8b0a_o-636x441.jpg" alt="47" height="441" width="636">
						</li><li class="postformat-galleryactive-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"> 
														<img draggable="false" src="profile_assets/public-domain-images-free-stock-photos-down-town-chicago-blue-sky-7-636x441.jpg" alt="47" height="441" width="636">
						</li></ul>
			<ol class="postformat-gallerycontrol-nav postformat-gallerycontrol-paging"><li><a class="">1</a></li><li><a class="">2</a></li><li><a class="">3</a></li><li><a class="postformat-galleryactive">4</a></li></ol><ul class="postformat-gallerydirection-nav"><li><a class="postformat-galleryprev" href="#"></a></li><li><a class="postformat-gallerynext" href="#"></a></li></ul></div>
			
			
			<script type="text/javascript">
				jQuery(window).load(function(){
					jQuery('.post-slider-47').flexslider({
						animation: "fade",
						namespace: "postformat-gallery",
						selector: ".slides > li",       
						easing: "swing",                
						direction: "vertical",
						slideshow: 'true',
						slideshowSpeed:'7000',        
						animationSpeed:'600',         
						controlsContainer: "",
						controlNav: 'true',       
						directionNav: 'true',    
						pauseOnHover: 'true',
						randomize:'true',
						prevText: "",
						nextText: ""
					});
				});
			</script> 
			
			
		</div>
		<!-- GALLERY ENDS -->
		
		<div class="skepost-meta">
			<span class="post-type-img"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-gallery-format.png"></span>
			<span class="date"><span>17</span><br>Jul,2014</span>
			<span class="author-name"><img class="avatar photo" src="profile_assets/Profile-pic-02.png"></span>
			<span class="comments"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/comment-box.png"><br><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/#respond" title="Comment on Gallery Post"><span>0</span></a></span>
			<span class="blog-post-like"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-meta-like.png"><br><span class="ul_cont" onclick="alter_ul_post_values(this,'47','_ldc_plc')"><span class="like_img ldc_img"></span> <span class="ldc_counts">20</span></span></span>
		</div><!-- skepost-meta -->
		
	</div><!--  -->

	
	<div class="skepost">
		<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum?</p>
 
		<div class="continue"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/">Read More</a></div>		  
	</div>
	<!-- skepost -->
	
	<div class="skt-blog-share clearfix">
		<ul>
			<!--Twitter-->
			<li class="twitter-icon"><a target="_blank" href="http://twitter.com/home?status=Gallery Posthttp://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/"><i class="fa fa-twitter"></i></a></li>
			<!--Facebook-->
			<li class="facebook-icon"><a target="_blank" href="http://www.facebook.com/sharer.php?u=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/&amp;t=Gallery Post"><i class="fa fa-facebook"></i></a></li>
			<!--Linked In-->
			<li class="linkedin-icon"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/&amp;title=Gallery Post&amp;summary=&amp;source="><i class="fa fa-linkedin"></i></a></li>
			<!--Google Plus-->
			<li class="gplusicon-icon"><a target="_blank" href="https://plus.google.com/share?url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/"><i class="fa fa-google-plus"></i></a></li>
			<!-- PINTEREST -->
			<li class="pinterest-icon"><a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media=&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/&amp;is_video=false&amp;description=Gallery Post"><i class="fa fa-pinterest"></i></a></li>
		</ul>
	</div><!-- skt-blog-share -->
	
</div>
<!-- post -->												
<div class="post-45 post type-post status-publish format-video hentry category-video tag-video-2 tag-youtube" id="post-45">

	<h1 class="post-title">
		<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/" title="Youtube Video Post">Youtube Video Post</a>
	</h1>
	  
	<div class="convac-post-box clearfix">
			<div class="video-container">
											<div class="flex-video widescreen vimeo">
								<iframe src="https://www.youtube.com/embed/ksvdvCDO7pA?wmode=opaque" class="youtube-video" allowfullscreen="" height="442" width="770"></iframe>
							</div>
									</div>
		
		
		<div class="skepost-meta clearfix">
			<span class="post-type-img"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-video-format.png"></span>
			<span class="date"><span>16</span><br>Jul,2014</span>
			<span class="author-name"><img class="avatar photo" src="profile_assets/Profile-pic-02.png"></span>
			<span class="comments"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/comment-box.png"><br><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/#respond" title="Comment on Youtube Video Post"><span>0</span></a></span>
			<span class="blog-post-like"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-meta-like.png"><br><span class="ul_cont" onclick="alter_ul_post_values(this,'45','_ldc_plc')"><span class="like_img ldc_img"></span> <span class="ldc_counts">10</span></span></span>
		</div><!-- skepost-meta -->
	</div><!--  -->

	<div class="skepost">
		<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of?</p>
 
		<div class="continue"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/">Read More</a></div>		  
	</div>
	<!-- skepost -->
	
	<div class="skt-blog-share clearfix">
		<ul>
			<!--Twitter-->
			<li class="twitter-icon"><a target="_blank" href="http://twitter.com/home?status=Youtube Video Posthttp://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/"><i class="fa fa-twitter"></i></a></li>
			<!--Facebook-->
			<li class="facebook-icon"><a target="_blank" href="http://www.facebook.com/sharer.php?u=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/&amp;t=Youtube Video Post"><i class="fa fa-facebook"></i></a></li>
			<!--Linked In-->
			<li class="linkedin-icon"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/&amp;title=Youtube Video Post&amp;summary=&amp;source="><i class="fa fa-linkedin"></i></a></li>
			<!--Google Plus-->
			<li class="gplusicon-icon"><a target="_blank" href="https://plus.google.com/share?url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/"><i class="fa fa-google-plus"></i></a></li>
			<!-- PINTEREST -->
			<li class="pinterest-icon"><a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media=&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/&amp;is_video=false&amp;description=Youtube Video Post"><i class="fa fa-pinterest"></i></a></li>
		</ul>
	</div><!-- skt-blog-share -->
		
</div>
<!-- post -->												
<div class="post-42 post type-post status-publish format-video hentry category-video tag-video-2" id="post-42">

	<h1 class="post-title">
		<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/" title="Vimeo Video Post">Vimeo Video Post</a>
	</h1>
	  
	<div class="convac-post-box clearfix">
			<div class="video-container">
									<div class="flex-video widescreen vimeo">
						<iframe src="https://player.vimeo.com/video/77972646?portrait=0" height="442" width="770"></iframe>
					</div>
							</div>
		
		
		<div class="skepost-meta clearfix">
			<span class="post-type-img"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-video-format.png"></span>
			<span class="date"><span>16</span><br>Jul,2014</span>
			<span class="author-name"><img class="avatar photo" src="profile_assets/Profile-pic-02.png"></span>
			<span class="comments"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/comment-box.png"><br><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/#respond" title="Comment on Vimeo Video Post"><span>0</span></a></span>
			<span class="blog-post-like"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-meta-like.png"><br><span class="ul_cont" onclick="alter_ul_post_values(this,'42','_ldc_plc')"><span class="like_img ldc_img"></span> <span class="ldc_counts">1</span></span></span>
		</div><!-- skepost-meta -->
	</div><!--  -->

	<div class="skepost">
		<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of?</p>
 
		<div class="continue"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/">Read More</a></div>		  
	</div>
	<!-- skepost -->
	
	<div class="skt-blog-share clearfix">
		<ul>
			<!--Twitter-->
			<li class="twitter-icon"><a target="_blank" href="http://twitter.com/home?status=Vimeo Video Posthttp://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/"><i class="fa fa-twitter"></i></a></li>
			<!--Facebook-->
			<li class="facebook-icon"><a target="_blank" href="http://www.facebook.com/sharer.php?u=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/&amp;t=Vimeo Video Post"><i class="fa fa-facebook"></i></a></li>
			<!--Linked In-->
			<li class="linkedin-icon"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/&amp;title=Vimeo Video Post&amp;summary=&amp;source="><i class="fa fa-linkedin"></i></a></li>
			<!--Google Plus-->
			<li class="gplusicon-icon"><a target="_blank" href="https://plus.google.com/share?url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/"><i class="fa fa-google-plus"></i></a></li>
			<!-- PINTEREST -->
			<li class="pinterest-icon"><a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media=&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/&amp;is_video=false&amp;description=Vimeo Video Post"><i class="fa fa-pinterest"></i></a></li>
		</ul>
	</div><!-- skt-blog-share -->
		
</div>
<!-- post -->												<div class="post-57 post type-post status-publish format-quote hentry category-quote tag-post tag-quote-2" id="post-57">

	<h1 class="post-title">
		<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/" title="Quote">Quote</a>
	</h1>

	<div class="convac-post-box clearfix">
	
		<div class="citation_post">
						<blockquote class="skt-quote">
				I think if you do something and it turns out pretty good, then you should go do something else wonderful, not dwell on it for too long. Just figure out what?s next.				<span class="quoteauthor">? <a href="http://www.sketchthemes.com/themes/convac-responsive-multi-author-blogging-theme/" title="Author" target="_blank">Steve Jobs</a></span>
			</blockquote>
		</div>
		
		<div class="skepost-meta">
			<span class="post-type-img"><img src="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/wp-content/themes/convac/images/blog-quote-format.png"></span>
			<span class="date"><span>12</span><br>Jul,2014</span>
			<span class="author-name"><img class="avatar photo" src="profile_assets/Profile-pic-02.png"></span>
			<span class="comments"><img src="profile_assets/comment-box.png"><br><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/#respond" title="Comment on Quote"><span>0</span></a></span>
			<span class="blog-post-like"><img src="profile_assets/blog-meta-like.png"><br><span class="ul_cont" onclick="alter_ul_post_values(this,'57','_ldc_plc')"><span class="like_img ldc_img"></span> <span class="ldc_counts">3</span></span></span>
		</div><!-- skepost-meta -->
		
	</div><!--  -->

	
	<div class="skepost">
		 
		<div class="continue"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/">Read More</a></div>		  
	</div>
	<!-- skepost -->	
	
	<div class="skt-blog-share clearfix">
		<ul>
			<!--Twitter-->
			<li class="twitter-icon"><a target="_blank" href="http://twitter.com/home?status=Quotehttp://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/"><i class="fa fa-twitter"></i></a></li>
			<!--Facebook-->
			<li class="facebook-icon"><a target="_blank" href="http://www.facebook.com/sharer.php?u=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/&amp;t=Quote"><i class="fa fa-facebook"></i></a></li>
			<!--Linked In-->
			<li class="linkedin-icon"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/&amp;title=Quote&amp;summary=&amp;source="><i class="fa fa-linkedin"></i></a></li>
			<!--Google Plus-->
			<li class="gplusicon-icon"><a target="_blank" href="https://plus.google.com/share?url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/"><i class="fa fa-google-plus"></i></a></li>
			<!-- PINTEREST -->
			<li class="pinterest-icon"><a target="_blank" href="http://pinterest.com/pin/create/bookmarklet/?media=&amp;url=http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/&amp;is_video=false&amp;description=Quote"><i class="fa fa-pinterest"></i></a></li>
		</ul>
	</div><!-- skt-blog-share -->

</div>
<!-- post -->																			<div class="navigation blog-navigation">	
								<div id="convac-paginate"><span class="convac-title"></span><span class="convac-page convac-current">1</span><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/page/2/#container" class="convac-page">2</a><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/page/2/#container" class="convac-next"><i class="fa fa-angle-right"></i></a><div class="clear"></div></div>					
							</div>  
							 
											</div>
					<!-- content -->
				  </div>
				  <!-- container --> 

				  <!-- Sidebar -->
				  <div id="sidebar" class="span4">
					<div id="sidebar_2" class="ske_widget">
	<ul class="skeside">
		<li id="search-4" class="ske-container widget_search"><h3 class="ske-title">Search</h3><form method="get" id="searchform" action="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/">
	<div class="searchleft">
		<input value="" placeholder="Search" name="s" id="searchbox" class="searchinput" type="text">
	</div>
    <div class="searchright">
    	<input class="submitbutton" value="Search" type="submit">
    </div>
    <div class="clearfix"></div>
</form>

</li><li id="sktrecentpostswidget-2" class="ske-container skt-recent-posts"><h3 class="ske-title">Recent Posts</h3>		<ul>
						<li class="clearfix"><span class="skt-rp-standard"></span><div class="skt-rp-item"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/standard-post/" title="Standard Post">Standard Post</a><div class="skt-rp-excerpt">Sed ut perspiciatis unde omnis iste natus error sit voluptatem</div></div></li>
						<li class="clearfix"><span class="skt-rp-gallery"></span><div class="skt-rp-item"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/gallery-post/" title="Gallery Post">Gallery Post</a><div class="skt-rp-excerpt">At vero eos et accusamus et iusto odio dignissimos ducimus</div></div></li>
						<li class="clearfix"><span class="skt-rp-video"></span><div class="skt-rp-item"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/youtube-video-post/" title="Youtube Video Post">Youtube Video Post</a><div class="skt-rp-excerpt">But I must explain to you how all this mistaken</div></div></li>
						<li class="clearfix"><span class="skt-rp-video"></span><div class="skt-rp-item"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/video-post/" title="Vimeo Video Post">Vimeo Video Post</a><div class="skt-rp-excerpt">But I must explain to you how all this mistaken</div></div></li>
						<li class="clearfix"><span class="skt-rp-quote"></span><div class="skt-rp-item"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/quote/" title="Quote">Quote</a></div></li>
				</ul>
			
					
		</li>        <li id="sktfollowwidget-2" class="ske-container SktFollowContact">		<h3 class="ske-title">Follow Us</h3>        <div class="follow-icons">
		<ul class="social clearfix"> 
		 <li class="linkedin-icon"> <a target="_blank" href="#" title="Linkedin"></a></li> 		 <li class="facebook-icon"> <a target="_blank" href="#" title="Facebook"></a></li> 		 <li class="twitter-icon"> <a target="_blank" href="#" title="Twitter"></a></li> 		 <li class="flickr-icon"> <a target="_blank" href="#" title="Flickr"></a></li> 		 <li class="gplusicon-icon"> <a target="_blank" href="#" title="Googleplus"></a></li> 		 <li class="skype-icon"> <a target="_blank" href="skype:#?call" title="Skype"></a></li> 		 <li class="youtube-icon"> <a target="_blank" href="#" title="Youtube"></a></li> 		 <li class="dribble-icon"> <a target="_blank" href="#" title="Dribble"></a></li> 		 <li class="pinterest-icon"> <a target="_blank" href="#" title="Pinterest"></a></li> 		 <li class="tumblr-icon"> <a target="_blank" href="#" title="Tumblr"></a></li> 		 <li class="github-icon"> <a target="_blank" href="#" title="Github"></a></li> 		 <li class="foursquare-icon"> <a target="_blank" href="#" title="Foursquare"></a></li> 		 </ul>
        <div class="clear"></div>
        </div>
        </li>        		<li id="sktsubswidget-2" class="ske-container subs-follow">		<h3 class="ske-title">Stay Connected</h3>			<form class="skt-subs-widget" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
				<input id="subs-widget-emailid" name="email" placeholder="Enter email address" type="text">
				<input value="" name="uri" type="hidden">
				<input name="loc" value="en_US" type="hidden">
				<input value="Submit" onclick="return SktSubsWidgetValid();" type="submit">
			</form>
		</li>                      <li id="sktvimeovideowidget-2" class="ske-container sktvimeovideo">                <h3 class="ske-title">Vimeo</h3>					<div class="skt-vimeo-video"><iframe src="http://player.vimeo.com/video/83019076?api=1&amp;player_id=vimeoplayer&amp;byline=1&amp;portrait=1&amp;autoplay=" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" height="190" width="328"></iframe></div>
                </li>              <li id="categories-2" class="ske-container widget_categories"><h3 class="ske-title">Categories</h3>		<ul>
	<li class="cat-item cat-item-17"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/audio/">Audio Post</a>
</li>
	<li class="cat-item cat-item-9"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/gallery/">Gallery Post</a>
</li>
	<li class="cat-item cat-item-15"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/link/">Link Post</a>
</li>
	<li class="cat-item cat-item-12"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/quote/">Quote Post</a>
</li>
	<li class="cat-item cat-item-19"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/sound-cloud/">Sound Cloud Post</a>
</li>
	<li class="cat-item cat-item-3"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/standard-post/">Standard Post</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/uncategorized/">Uncategorized</a>
</li>
	<li class="cat-item cat-item-5"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/category/video/">Video Post</a>
</li>
		</ul>
</li><li id="calendar-3" class="ske-container widget_calendar"><h3 class="ske-title">Calendar</h3><div id="calendar_wrap"><table id="wp-calendar">
	<caption>January 2015</caption>
	<thead>
	<tr>
		<th scope="col" title="Monday">M</th>
		<th scope="col" title="Tuesday">T</th>
		<th scope="col" title="Wednesday">W</th>
		<th scope="col" title="Thursday">T</th>
		<th scope="col" title="Friday">F</th>
		<th scope="col" title="Saturday">S</th>
		<th scope="col" title="Sunday">S</th>
	</tr>
	</thead>

	<tfoot>
	<tr>
		<td colspan="3" id="prev"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/2014/07/">� Jul</a></td>
		<td class="pad">&nbsp;</td>
		<td colspan="3" id="next" class="pad">&nbsp;</td>
	</tr>
	</tfoot>

	<tbody>
	<tr>
		<td colspan="3" class="pad">&nbsp;</td><td>1</td><td>2</td><td>3</td><td>4</td>
	</tr>
	<tr>
		<td>5</td><td>6</td><td>7</td><td>8</td><td id="today">9</td><td>10</td><td>11</td>
	</tr>
	<tr>
		<td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td><td>18</td>
	</tr>
	<tr>
		<td>19</td><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td>
	</tr>
	<tr>
		<td>26</td><td>27</td><td>28</td><td>29</td><td>30</td><td>31</td>
		<td class="pad" colspan="1">&nbsp;</td>
	</tr>
	</tbody>
	</table></div></li><li id="tag_cloud-2" class="ske-container widget_tag_cloud"><h3 class="ske-title">Tags</h3><div class="tagcloud"><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/audio/" class="tag-link-17" title="1 topic" style="font-size: 8pt;">Audio Post</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/comments/" class="tag-link-20" title="1 topic" style="font-size: 8pt;">Comments</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/gallery/" class="tag-link-9" title="1 topic" style="font-size: 8pt;">Gallery Post</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/link/" class="tag-link-15" title="1 topic" style="font-size: 8pt;">Link Post</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/post/" class="tag-link-11" title="3 topics" style="font-size: 22pt;">post</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/quote-2/" class="tag-link-14" title="1 topic" style="font-size: 8pt;">quote</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/sound-cloud/" class="tag-link-19" title="1 topic" style="font-size: 8pt;">Sound Cloud Post</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/standard/" class="tag-link-4" title="1 topic" style="font-size: 8pt;">Standard</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/video-2/" class="tag-link-7" title="2 topics" style="font-size: 16.4pt;">video</a>
<a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/tag/youtube/" class="tag-link-8" title="1 topic" style="font-size: 8pt;">youtube</a></div>
</li><li id="archives-2" class="ske-container widget_archive"><h3 class="ske-title">Archives</h3>		<ul>
	<li><a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/2014/07/">July 2014</a></li>
		</ul>
</li><li id="recent-comments-2" class="ske-container widget_recent_comments"><h3 class="ske-title">Recent Comments</h3><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link"><a href="http://www.sketchthemes.com/" rel="external nofollow" class="url">Sketch</a></span> on <a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/sound-cloud-post/#comment-10">Sound Cloud Post</a></li><li class="recentcomments"><span class="comment-author-link"><a href="http://www.sketchthemes.com/" rel="external nofollow" class="url">Sketch</a></span> on <a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/template-comments/#comment-9">Template: Comments</a></li><li class="recentcomments"><span class="comment-author-link"><a href="http://www.sketchthemes.com/" rel="external nofollow" class="url">Sketch</a></span> on <a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/template-comments/#comment-8">Template: Comments</a></li><li class="recentcomments"><span class="comment-author-link"><a href="http://www.sketchthemes.com/" rel="external nofollow" class="url">Sketch</a></span> on <a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/template-comments/#comment-7">Template: Comments</a></li><li class="recentcomments"><span class="comment-author-link"><a href="http://www.sketchthemes.com/" rel="external nofollow" class="url">Sketch</a></span> on <a href="http://sketchthemes.com/samples/convac-multi-author-blogging-demo/template-comments/#comment-6">Template: Comments</a></li></ul></li>	</ul>
</div>
<!-- #sidebar_2 .ske_widget -->
 



				  </div>
				  <!-- Sidebar --> 
			 </div><!-- row-fluid -->
		 </div><!-- container -->
	</div>
</div>

	<div class="clearfix"></div>
</div>
<!-- #main --> 

<!-- .twitter slider section starts -->
<div id="full-twitter-box">
	<div class="container">
		<div class="row-fluid">
			    <div id="bot-twitter">
            <div class="twitter-row">
                				
                <div class="tw-slider clearfix">
                    <ul class="twitter_box slides">
						                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">6 Reasons Why All Bloggers should switch to WordPress in 2015! <a href="http://t.co/kADQ2LRFxB" target="_blank" rel="nofollow">http://t.co/kADQ2LRFxB</a> 

#SketchThemes #blogger... <a href="http://t.co/zqpaqyxfy5" target="_blank" rel="nofollow">http://t.co/zqpaqyxfy5</a></span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=553441861390848000;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=553441861390848000;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=553441861390848000;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									9 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">InCart - SketchThemes continues the legacy of best multipurpose responsive business WordPress themes.... <a href="http://t.co/iXpMbjophe" target="_blank" rel="nofollow">http://t.co/iXpMbjophe</a></span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=553066221542055936;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=553066221542055936;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=553066221542055936;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									8 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">InCart - SketchThemes continues the legacy of best multipurpose responsive business WordPress themes.  <a href="http://t.co/z6cRviU7GF" target="_blank" rel="nofollow">http://t.co/z6cRviU7GF</a> @SketchThemes</span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=553039303501025281;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=553039303501025281;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=553039303501025281;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									8 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">ADVERTICA - A creative, minimal and responsive WordPress Theme, for creatives and professionals alike.... <a href="http://t.co/lMihtJ0zME" target="_blank" rel="nofollow">http://t.co/lMihtJ0zME</a></span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=552721679534002178;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=552721679534002178;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=552721679534002178;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									7 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">ADVERTICA, a creative, minimal and responsive WordPress Theme, for creatives and professionals alike. <a href="http://t.co/EGf7TDUBj0" target="_blank" rel="nofollow">http://t.co/EGf7TDUBj0</a> @SketchThemes</span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=552720414502170624;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=552720414502170624;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=552720414502170624;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									7 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">Incart Lite is now available live at <a href="http://t.co/QiBWqCR6z6" target="_blank" rel="nofollow">http://t.co/QiBWqCR6z6</a> .  <a href="http://t.co/OnyULGPVTW" target="_blank" rel="nofollow">http://t.co/OnyULGPVTW</a>  #WooCommerce @SketchThemes <a href="http://t.co/CwjZ5LMFVC" target="_blank" rel="nofollow">http://t.co/CwjZ5LMFVC</a></span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=552432104928468993;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=552432104928468993;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=552432104928468993;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									6 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">Eptima - Corporate Responsive WordPress Theme!!!  <a href="http://t.co/MzuvVMqKE7" target="_blank" rel="nofollow">http://t.co/MzuvVMqKE7</a>  #WordPress #corporate @SketchThemes</span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=552329320375787523;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=552329320375787523;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=552329320375787523;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									6 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="twitter-item foot-tw-active-slide">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">Eptima Lite is now available live at  <a href="http://t.co/asgCDYBuni" target="_blank" rel="nofollow">http://t.co/asgCDYBuni</a>  @SketchThemes #corporate #business #wordpress <a href="http://t.co/o364IkoVY5" target="_blank" rel="nofollow">http://t.co/o364IkoVY5</a></span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=552055816660873217;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=552055816660873217;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=552055816660873217;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									5 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">SketchThemes has come up with cool ?Happy Theme? offer this New Year which unfolds all its fresh, elegant and... <a href="http://t.co/yWCSFxjznu" target="_blank" rel="nofollow">http://t.co/yWCSFxjznu</a></span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=551280366053761025;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=551280366053761025;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=551280366053761025;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									3 Jan 2015                                </span>
                            </li>
                                                    <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="twitter-item">
								<i class="fa fa-twitter"></i><br>
								<span class="tw-usernm">Sketch Themes</span>                                <span class="tw-text">Advertica - The Uber Clean MultiPurpose WordPress Theme!!!  <a href="http://t.co/EGf7TDUBj0" target="_blank" rel="nofollow">http://t.co/EGf7TDUBj0</a> #wordpress #clean @SketchThemes</span>								<div class="tw-controls">                                <a href="http://twitter.com/intent/tweet?in_reply_to=551266832355561474;" class="tweet_action tweet_reply" target="_blank">reply</a>                                <a href="http://twitter.com/intent/retweet?tweet_id=551266832355561474;" class="tweet_action tweet_retweet" target="_blank">retweet</a>                                <a href="http://twitter.com/intent/favorite?tweet_id=551266832355561474;" class="tweet_action tweet_favorite" target="_blank">favorite</a>								</div>                                <span class="tw-date">
									3 Jan 2015                                </span>
                            </li>
                        						                    </ul>
                <ol class="foot-tw-control-nav foot-tw-control-paging"><li><a class="">1</a></li><li><a class="">2</a></li><li><a class="">3</a></li><li><a class="">4</a></li><li><a class="">5</a></li><li><a class="">6</a></li><li><a class="">7</a></li><li><a class="foot-tw-active">8</a></li><li><a class="">9</a></li><li><a class="">10</a></li></ol></div>
            </div>
            <div class="nav"></div>


 </div> 		</div>
	</div>
</div>
<!-- .twitter slider section ends -->

<!-- #footer -->
<div id="footer">
	<div class="foot_wrapper">
		<div class="container">
			<div class="row-fluid">
								<p>Copyrights � 2014 <a href="#">Convac</a></p>				<div class="skt-foot-link">Convac Theme by <a href="http://www.sketchthemes.com/themes/convac-responsive-multi-author-blogging-theme/" target="_blank">SketchThemes</a></div>
			</div>
		</div>
	</div><!-- third_wrapper --> 
</div>
<!-- #footer -->

</div>
<!-- #wrapper -->
	<a style="display: none;" href="JavaScript:void(0);" title="Back To Top" id="backtop"></a>
	<!-- Start analytics code 16/10/2014 3:51:02 PM -->

<!-- End analytics code 16/10/2014 3:51:09 PM -->
		<!-- SketchThemes but button section starts -->
			<style type="text/css">
				#sktbuybtn{right:0;top:49% }
				#sktbuybtn a.sktbuybtn_button {color:#FFFFFF;background-color:#1588FF}
			</style>
			<div id="sktbuybtn"><a href="http://www.sketchthemes.com/members/signup/convac" target="_blank" class="sktbuybtn_button">Buy Theme</a></div>
		<!-- SketchThemes but button section ends -->
		




<div id="tiptip_holder" style="max-width:200px;"><div id="tiptip_arrow"><div id="tiptip_arrow_inner"></div></div><div id="tiptip_content"></div></div></body>

</html>